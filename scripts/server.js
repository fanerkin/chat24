/*eslint no-unused-vars: 0*/
var fs = require('fs')
var http = require('http')
var path = require('path')

var xtend = require('xtend')
var log = require('./log')

var concat = require('concat-stream')
var formidable = require('formidable')
var router = require('routes')()
var body = require('body/any')
var ecstatic = require('ecstatic')
var st = ecstatic({
	root: __dirname + '/../dist',
	cache: 0,
	showDir: false,
	serverHeader: false
})

var response = {
	oid: 299,
	name: 'Vasia',
	room: 0,
	last_message: 1,
	messages: [
		{
			oid: 1,
			time: 1436726355,
			room: 0,
			appeal: false,
			private: false,
			oid_from: 299,
			name_from: 'Vasia',
			name_to: '',
			images: [],
			audios: [],
			youtube: [],
			html: 'ERROR',
			raw: 'ERROR'
		}
	],
	users: [
		{
			oid: 0,
			name: 'Robot'
		},
		{
			oid: 299,
			name: 'Vasia'
		}
	],
	rooms: [
		{
			oid: 0,
			name: '#Courtyard',
			inside: true,
			private: false,
			allow: false,
			amount: 5
		}
	],
	images: [],
	audios: [],
	info: '',
	error: ''
}

if (process.env.NODE_ENV === 'production') {
	console.log('dev only! )')
	process.exit(1)
}

router.addRoute('/view.json*', get((req, res, params) => {
	res.setHeader('content-type', 'application/json')
	res.end(JSON.stringify(response))
}))

router.addRoute('/personal.json*', get((req, res, params) => {
	res.setHeader('content-type', 'application/json')
	res.end(JSON.stringify({messages: []}))
}))

router.addRoute('/*.json', post(rawBody, (req, res, params) => {
	res.setHeader('content-type', 'application/json')
	res.end(JSON.stringify({length: params.body.length}))
}))

http.createServer((req, res) => {
	var match = router.match(req.url)
	if (match) {
		match.fn(req, res, match.params)
	} else {
		res.statusCode = 404
		res.end('NOT FOUND\n')
	}
})
.listen(8888, () => {
	console.log('listening on http://localhost:8888')
})

function get (fn) {
	return function (req, res, params) {
		if (req.method !== 'GET') {
			res.statusCode = 400
			res.end('GET?\n')
		} else {
			fn(req, res, params)
		}
	}
}

function post (parser, fn) {
	return function (req, res, params) {
		if (req.method !== 'POST') {
			res.statusCode = 400
			res.end('POST?\n')
		} else {
			parser(req, res, (err, body) => {
				if (err) {
					return log.error(err)
				}
				fn(req, res, xtend(params, {body}))
			})
		}
	}
}

function rawBody (req, res, next) {
	req.pipe(concat(body => {
		next(null, body)
	}))
}

// TODO revert from middleware to callback before using
function uploadParser (req, res, next) {
	var form = new formidable.IncomingForm({
		// FIXME disable for heroku
		uploadDir: path.join(__dirname, '..', 'tmp'),
		keepExtensions: true
	})

	form.parse(req, (err, fields, files) => {
		if (err) {
			return next(err)
		}
		var formData = xtend(fields)

		formData.file = {
			value: fs.createReadStream(files.file.path),
			options: {
				filename: files.file.name
			}
		}
		req.body = formData
		req.filePath = files.file.path
		next()
	})
}

function unlink (path) {
	fs.unlink(path, () => {
		console.log('deleted tmp file')
	})
}

#!/usr/bin/env bash

set -u
set -e

# Create your very own Root Certificate Authority
openssl genrsa \
	-out tmp/my-root-ca.key.pem \
	2048

# Self-sign your Root Certificate Authority
# Since this is private, the details can be as bogus as you like
openssl req \
	-x509 \
	-new \
	-sha256 \
	-nodes \
	-key tmp/my-root-ca.key.pem \
	-days 30 \
	-out tmp/my-root-ca.crt.pem \
	-subj "/C=US/ST=CA/L=SF/O=test/CN=authority.com"

# NOTE
# -nodes means "no-des" which means "no passphrase"

#########

FQDN='localhost'

# Create Certificate for this domain,
openssl genrsa \
	-out tmp/my-server.key.pem \
	2048

# Create the CSR
openssl req -new \
	-key tmp/my-server.key.pem \
	-out tmp/my-server.csr.pem \
	-subj "/C=US/ST=CA/L=SF/O=test/CN=${FQDN}"

##############


# Sign the request from Device with your Root CA
openssl x509 \
	-req -in tmp/my-server.csr.pem \
	-sha256 \
	-CA tmp/my-root-ca.crt.pem \
	-CAkey tmp/my-root-ca.key.pem \
	-CAcreateserial \
	-out tmp/my-server.crt.pem \
	-days 30

rm tmp/my-server.csr.pem

# If you already have a serial file, you would use that (in place of CAcreateserial)
# -CAserial certs/ca/my-root-ca.srl

# openssl genrsa -aes128 -out tmp/cert.pass.key 1024
# openssl rsa -in tmp/cert.pass.key -out tmp/cert.key
# rm tmp/cert.pass.key 
# openssl req -new -key tmp/cert.key -out tmp/cert.csr \
# 	-subj "/C=US/ST=California/L=SF/O=test/CN=localost"
# openssl x509 -req -days 30 -in tmp/cert.csr \
# 	-signkey tmp/cert.key -out tmp/cert.crt
# rm tmp/cert.csr

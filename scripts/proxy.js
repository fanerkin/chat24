/*eslint no-unused-vars: 0*/
var fs = require('fs')
var http = require('http')
var https = require('https')

var log = require('./log')
var ecstatic = require('ecstatic')
var st = ecstatic({
	root: __dirname + '/../dist',
	cache: 0,
	showDir: false,
	serverHeader: false
})

var reErrors = /(ECONNREFUSED|ENOTFOUND|ETIMEDOUT|ESOCKETTIMEDOUT|EHOSTUNREACH)/
var port
var pro = port ? http : https
var host = port ? 'localhost' : 'horn.chat'

function getNewHeaders (req) {
	var h = {}
	if (req.method === 'POST' && !req.headers['content-length']) {
		log.info('no content-length')
	}
	Object.keys(req.headers).forEach((header) => {
		if (header === 'referer') {
			h[header] = 'https://horn.chat/'
			return
		} else if (header === 'connection') {
			h[header] = 'close'
			return
		}

		h[header] = req.headers[header]
	})
	if (/localhost/.test(h.host)) {
		h.host = host
	}
	return h
}

function onError (err, req, res) {
	if (reErrors.test(err.message)) {
		console.log(err.message)
	} else {
		log.error(err)
	}
	res.writeHead(502, {
		'content-type': 'text/plain; charset=utf-8'
	})
	res.end('from: node\nmessage: ' + (err.message || err))
}

function checkAborted (resp, res, isAborted) {
	if (isAborted) {
		console.log('that request was aborted')
		res.end()
	} else {
		res.writeHead(resp.statusCode, getNewHeaders(resp))
		resp.pipe(res)
	}
}

function app (req, res) {
	var relay
	var delay
	var start
	var isAborted = false
	var options = {
		method: req.method,
		host,
		path: req.url,
		headers: getNewHeaders(req)
	}
	if (port) {
		options.port = port
	}

	switch (req.method) {
	case 'OPTIONS':
	case 'GET':
	case 'HEAD':
		st(req, res, () => {
			pro.request(options, resp => {
				checkAborted(resp, res, isAborted)
			})
			.on('error', (err) => {
				onError(err, req, res)
			})
			.end()
		})
		break
	case 'POST':
		start = new Date()
		relay = pro.request(options, resp => {
			delay = new Date() - start
			setTimeout(() => {
				checkAborted(resp, res, isAborted)
			}, delay >= 1000 ? 0 : 1000 - delay)
		})
		.on('error', (err) => {
			onError(err, req, res)
		})

		// req.pipe(process.stdout)
		req.pipe(relay)
		break
	default:
		res.writeHead(405, {
			allow: 'OPTIONS, GET, HEAD, POST'
		})
		res.end('not allowed\n')
	}
	req.on('close', () => isAborted = true)
}

var key, cert
try {
	key = fs.readFileSync('tmp/my-server.key.pem')
	cert = fs.readFileSync('tmp/my-server.crt.pem')
} catch (e) {
	console.log(e.message)
	process.exit(1)
}

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
var insecurePort = process.env.PORT || 8181
var securePort = 8443
https.createServer({
	key,
	cert,
	requestCert: false,
	rejectUnauthorized: false
}, app)
.listen(securePort, () => {
	console.log('listening on https://localhost:' + securePort)
})

http.createServer(app)
.listen(insecurePort, () => {
	console.log('listening on http://localhost:' + insecurePort)
})

// http.createServer((req, res) => {
// 	res.writeHead(301, {
// 		'location': 'https://localhost:' + securePort + (req.path || ''),
// 		'cache-control': 'no-cache',
// 		'content-length': '0'
// 	})
// 	res.end('redirecting')
// })
// .listen(insecurePort, () => {
// 	console.log('listening on http://localhost:' + insecurePort)
// })

if (port === 8888) {
	require('child_process').fork(__dirname + '/server')
}

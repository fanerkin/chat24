#!/usr/bin/env bash

if [ ! -d tmp ]; then
	mkdir tmp
fi
cat src/css/*.css | \
# one line comments, multiline comments, newlines and tabs, spaces
sed -e 's/\/\*.*\*\///g
	/\/\*/,/\*\//d' | \
tr -d '\n\t' | \
sed -e 's/ *; *} */}/g
	s/ *\(!imp\|[>:,{]\) */\1/g' > tmp/build.css

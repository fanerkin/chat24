'use strict'

var fs = require('fs')
var path = require('path')

try {
	if (!fs.statSync('log').isDirectory()) {
		console.log('log is present but is not a directory')
		process.exit(20)
	}
} catch (err) {
	fs.mkdirSync('log')
}
var bunyan = require('bunyan')
var log = bunyan.createLogger({
	name: 'chat24',
	streams: [
		{
			level: 'error',
			type: 'rotating-file',
			path: path.join('log', 'error.log'),
			period: '1d',
			count: 3
		},
		{
			level: 'info',
			type: 'rotating-file',
			path: path.join('log', 'info.log'),
			period: '1d',
			count: 3
		},
		{
			level: 'info',
			stream: process.stdout
		}
	]
})

;['fatal', 'error', 'warn', 'info', 'debug', 'trace'].forEach(m => {
	log[m] = log[m].bind(log)
})

module.exports = log

/*eslint no-unused-vars: 0*/
var fs = require('fs')
var path = require('path')

var log = require('./log')
var browserify = require('browserify')
var watchify = require('watchify')
var babelify = require('babelify')
var brfs = require('brfs')

var express = require('express')
var app = express()
var st = express.static(path.join(__dirname, '..', 'dist'))
var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({extended: true})
var formidable = require('formidable')

var response = {
	oid: 299,
	name: 'Vasia',
	room: 0,
	last_message: 1,
	messages: [
		{
			oid: 1,
			time: 1436726355,
			room: 0,
			appeal: false,
			private: false,
			oid_from: 299,
			name_from: 'Vasia',
			name_to: '',
			images: [],
			audios: [],
			youtube: [],
			html: 'ERROR',
			raw: 'ERROR'
		}
	],
	users: [
		{
			oid: 0,
			name: 'Robot'
		},
		{
			oid: 299,
			name: 'Vasia'
		}
	],
	rooms: [
		{
			oid: 0,
			name: '#Courtyard',
			inside: true,
			private: false,
			allow: false,
			amount: 5
		}
	],
	images: [],
	audios: [],
	hash_images: 0,
	hash_audio: 0,
	info: '',
	error: ''
}

app.set('port', (process.env.PORT || 8182))
;['etag', 'x-powered-by'].forEach(field => app.disable(field))

var b = browserify(path.join(__dirname, '..', 'test', 'index.js'))
var w = watchify(b)
w.transform(babelify)
w.transform(brfs)

app.get('/test.js', (req, res, next) => {
	w.bundle((err, js) => {
		if (err) {
			return next(err)
		}

		res.set('cache-control', 'private, no-cache')
		res.set('content-type', 'application/javascript')
		res.set('content-length', js.length)
		res.end(js)
	})
})

app.get('/', (req, res, next) => {
	res.set('content-type', 'text/html; charset=utf-8')
	fs.createReadStream(path.join(__dirname, '..', 'dist', 'index-test.html'))
		.on('error', next)
		.pipe(res)
})

app.get('/view.json*', (req, res, next) => {
	res.set('content-type', 'application/json; charset=utf-8')
	res.end(JSON.stringify(response))
})

app.get('/*', st)

app.use((err, req, res, next) => {
	log.error(err)
	next(err)
})

app.use((err, req, res, next) => {
	res.writeHead(500, {
		'content-type': 'text/plain; charset=utf-8'
	})
	res.send('from node\nmessage: ' + (err.message || err))
})

app.listen(app.get('port'), () => {
	console.log('listening on http://localhost:' + app.get('port'))
})

'use strict'

import element from 'virtual-element'
import {Button} from '../Button'

function onFileSelected (e, c) {
	let {store} = c.props
	let {convert} = store.tools.image
	let {API} = store

	if (!e.target.value) {
		return
	}
	let wrapper = document.querySelector('.file-uploader')
	let input = wrapper.querySelector('.uploader-input')
	let file = input.files[0]
	let fd = new FormData()

	wrapper.classList.remove('show')
	wrapper.classList.add('hide')

	if (/image\//.test(file.type) && !/\/gif/.test(file.type)) {
		convert(file, blob => {
			fd.append('file', blob, `${file.name}.jpg`)
			fd.append('original_name', file.name)
			API.file.upload(fd, cb)
		})
	} else {
		fd.append('file', file)
		fd.append('original_name', file.name)
		API.file.upload(fd, cb)
	}

	function cb () {
		input.value = null
		wrapper.classList.remove('hide')
		wrapper.classList.add('show')
	}
}

let Upload = {
	name: 'Upload',

	render (c) {
		let {store, lang} = c.props
		let {i18n} = store.tools
		return (
			<div class="file-uploader">
				<input
					class="uploader-input"
					type="file"
					name="image"
					onChange={onFileSelected}
				/>
				<Button
					store={store}
					fa="fa-upload"
					label="^"
					labelBottom={i18n(lang, '007')}
					className="ghost"
				/>
			</div>
		)
	}
}

export default {Upload}

'use strict'

import element from 'virtual-element'
import {Message} from './Message'

let Personal = {
	name: 'Personal',

	shouldUpdate (c, nextProps) {
		return c.props.store.tools.shouldUpdate(c, nextProps)
	},

	render (c) {
		let {props} = c
		let {store, roomState,
			isSourceHtml, lang, oid} = props

		let children = roomState.messageStacks
			.map(messageStack => {
				let message = messageStack[0]
				let own = message.oid_from !== 0 && message.oid_from === oid
				return (
					<Message
						store={store}
						isSourceHtml={isSourceHtml}
						isTyping={false}
						roomOid={'personal'}
						roomState={roomState}
						messageStack={messageStack}
						message={message}
						own={own}
						lang={lang}
					/>
				)
			})

		return (
			<div class={`room room_oid_personal`}>
				<span class="name click">
					personal messages
				</span>
				<div class="messages">
					{children}
				</div>
			</div>
		)
	}
}

export default {Personal}

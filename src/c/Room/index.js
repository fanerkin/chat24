'use strict'

import element from 'virtual-element'
import {Message} from './Message'
import {Send} from './Send'

function onLeave (e, c) {
	let {store, room} = c.props
	let {API} = store
	API.room.leave({room: room.oid})
}

let Room = {
	name: 'Room',

	shouldUpdate (c, nextProps) {
		return c.props.store.tools.shouldUpdate(c, nextProps)
	},

	render (c) {
		let {props} = c
		let {store, roomState, room, isOutsideOfRooms,
			isSourceHtml, writers, lang, name, oid,
			images, audios, roomStates, isTyped} = props
		let {i18n} = store.tools
		let children = roomState.messageStacks
			.map(messageStack => {
				let message = messageStack[0]
				let own = message.oid_from !== 0 && message.oid_from === oid
				return (
					<Message
						store={store}
						isSourceHtml={isSourceHtml}
						isTyping={!!writers
							.filter(writer =>
								writer.oid === messageStack[0].oid_from &&
								writer.oid !== oid
							)
							.length}
						roomOid={room.oid}
						roomState={roomState}
						messageStack={messageStack}
						message={message}
						own={own}
						lang={lang}
					/>
				)
			})
		return (
			<div class={`room room_oid_${room.oid} active`}>
				<div class="list list-item"
					onClick={onLeave}
				>
					<span class="name">
						{room.name}
					</span>
					<span class="value">
						{room.amount}
					</span>
				</div>
				<div class="messages">
					{children}
				</div>
				{name
					? (isOutsideOfRooms
						? i18n(lang, '038')
						: <Send
							store={store}
							roomState={roomState}
							roomOid={room.oid}
							lang={lang}
							images={images}
							audios={audios}
							roomStates={roomStates}
							isTyped={isTyped}
						/>
					)
					: i18n(lang, '039')
				}
			</div>
		)
	}
}

export default {Room}

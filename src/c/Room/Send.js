'use strict'

import element from 'virtual-element'
import {Button} from '../Button'
import {Upload} from './Upload'
import {Smiles} from './Smiles'
import {Pictures} from './Pictures'
import {Audios} from './Audios'

let setTyping = (() => {
	let timeout

	return function setTyping (store, isTyping) {
		let o = store.getState()
		if (isTyping) {
			if (timeout) {
				clearTimeout(timeout)
			}
			if (!o.isTyping) {
				store.dispatch({
					type: 'SET_TYPING',
					value: true
				})
			}
			timeout = setTimeout(() => {
				setTyping(store, false)
			}, 5000)
		} else {
			if (timeout) {
				clearTimeout(timeout)
			}
			if (o.isTyping) {
				store.dispatch({
					type: 'SET_TYPING',
					value: false
				})
			}
		}
	}
})()

function createData (store, roomOid, message) {
	let o = store.getState()
	let {audioOids} = o.roomStates[roomOid]
	let {imageOids} = o.roomStates[roomOid]
	let data = {
		room: roomOid,
		message
	}
	if (imageOids.size) {
		data.images = Array.from(imageOids)
	}
	if (audioOids.size) {
		data.audios = Array.from(audioOids)
	}
	if (o.roomStates[roomOid].poid != null) {
		data.poid = o.roomStates[roomOid].poid
	}
	return data
}

function onSent (store, ta, roomOid, responseText) {
	store.dispatch({
		type: 'SET_MESSAGE_MODE',
		mode: 'empty',
		room: roomOid
	})
	ta.value = ''
	ta.classList.remove('dim', 'cur-wait')
	ta.disabled = false
	// blurs in chrome, needs refocus
	ta.focus()
	store.dispatch({
		type: 'SET_TEXT_TYPED',
		room: roomOid,
		isTyped: !!ta.value.length
	})
	return responseText
}

function onFail (ta, value) {
	ta.classList.remove('dim', 'cur-wait')
	ta.disabled = false
	// blurs in chrome, needs refocus
	ta.focus()
	return value
}

function onKeyDown (e, c) {
	if (
		e != null &&
		e.keyCode === 13
	) {
		onSend(e, c)
		return
	}
}

function onInput (e, c) {
	let {store, roomOid} = c.props
	let ta = e.target

	store.dispatch({
		type: 'SET_TEXT_TYPED',
		room: roomOid,
		isTyped: !!ta.value.length
	})

	setTyping(store, true)
}

function onSend (e, c) {
	let {store, roomOid} = c.props
	let {API} = store
	let ta = document.querySelector(`.type-message`)
	setTyping(store, false)
	ta.classList.add('dim', 'cur-wait')
	ta.disabled = true
	API.message.send(
		createData(store, roomOid, ta.value),
		onSent.bind(null, store, ta, roomOid),
		onFail.bind(null, ta)
	)
}

function onCancel (e, c) {
	let {store, roomState, roomOid} = c.props

	if (roomState.mode !== 'private' && roomState.mode !== 'edit') {
		let ta = document.querySelector(`.type-message`)
		ta.value = ''
		store.dispatch({
			type: 'SET_MESSAGE_MODE',
			mode: 'public + empty',
			room: roomOid
		})
		store.dispatch({
			type: 'SET_TEXT_TYPED',
			room: roomOid,
			isTyped: !!ta.value.length
		})
	} else {
		store.dispatch({
			type: 'SET_MESSAGE_MODE',
			mode: 'public',
			room: roomOid
		})
	}
}

function onSmiles (e, c) {
	let {roomOid, store} = c.props
	let {dom} = store
	let o = store.getState()
	let ta = document.querySelector(`.type-message`)
	ta.dispatchEvent(new Event('blur'))
	dom.choose({
		// Smiles is in another tree
		c: <Smiles smiles={o.defaultSmiles}/>
	}, e => {
		if (e.target.tagName.toLowerCase() !== 'img') {
			ta.dispatchEvent(new Event('focus'))
			return true
		}
		ta.value = `${ta.value} ${e.target.alt} `
		store.dispatch({
			type: 'SET_TEXT_TYPED',
			room: roomOid,
			isTyped: !!ta.value.length
		})
		document.querySelector('.close').click()
	})
}

function onAttach (e, c) {
	let {store, roomOid} = c.props
	let {API, dom} = store
	let {i18n} = store.tools
	let o = store.getState()
	let images = o.roomStates[roomOid].imageOids
	let audios = o.roomStates[roomOid].audioOids

	let ta = document.querySelector(`.type-message`)
	ta.dispatchEvent(new Event('blur'))
	dom.choose({
		c: (
			<div class="modal-inner">
				<Audios audios={audios}
					oaudios={o.audios}
					roomOid={roomOid}
				/>
				<Pictures images={images}
					oimages={o.images}
					roomOid={roomOid}
				/>
				<Button
					fa="fa-trash"
					store={store}
					labelBottom={i18n(o.lang, '029')}
					className="del"
				/>
			</div>
		)
	}, e => {
		// TODO extract these
		function onAudio (e) {
			let o = store.getState()
			let {audioOids} = o.roomStates[roomOid]
			let audioOid = +e.target.getAttribute('data-oid')
			if (audioOids.has(audioOid)) {
				store.dispatch({
					type: 'ATTACHMENT',
					operation: 'delete',
					item: 'audio',
					room: roomOid,
					oid: audioOid
				})
				e.target.classList.remove('o-green')
			} else {
				store.dispatch({
					type: 'ATTACHMENT',
					operation: 'add',
					item: 'audio',
					room: roomOid,
					oid: audioOid
				})
				e.target.classList.add('o-green')
			}
		}
		function onImage (e) {
			let o = store.getState()
			let {imageOids} = o.roomStates[roomOid]
			let imageOid = +e.target.className
				.replace(/.*oid_(\d+).*/, '$1')
			if (imageOids.has(imageOid)) {
				store.dispatch({
					type: 'ATTACHMENT',
					operation: 'delete',
					item: 'image',
					room: roomOid,
					oid: imageOid
				})
				e.target.classList.remove('o-green')
			} else {
				store.dispatch({
					type: 'ATTACHMENT',
					operation: 'add',
					item: 'image',
					room: roomOid,
					oid: imageOid
				})
				e.target.classList.add('o-green')
			}
		}

		function onDelete () {
			let o = store.getState()
			let roomState = o.roomStates[roomOid]
			let images = roomState.imageOids
			let audios = roomState.audioOids
			let deletables = []
			if (audios.size) {
				deletables.push({
					object: 'audio',
					oids: Array.from(audios)
				})
			}
			if (images.size) {
				deletables.push({
					object: 'image',
					oids: Array.from(images)
				})
			}
			if (deletables.length) {
				API.file.delete({
					actions: deletables
				})
			}
		}

		let classList = e.target.parentNode.classList
		if (classList.contains('audio')) {
			return onAudio(e)
		} else if (classList.contains('picture')) {
			return onImage(e)
		} else if (classList.contains('del') ||
			classList.contains('label')) {
			return onDelete(e)
		}
	})
}

function onFocus () {
	setTimeout(() => {
		document.dispatchEvent(new Event('scrollToBottom'))
	}, 300)
}

function getClassForTextarea (roomState) {
	return roomState.poid
		? ' shadow-green bg-green b-green'
		: roomState.to != null
			? ' shadow-red bg-red b-red'
			: ''
}

let Send = {
	name: 'Send',

	shouldUpdate (c, nextProps) {
		return c.props.store.tools.shouldUpdate(c, nextProps)
	},

	render (c) {
		let {props} = c
		let {store, roomState, roomOid,
			lang, images, audios, roomStates, isTyped} = props
		let {i18n} = store.tools

		return (
			<div class="message-adding">
				<Button
					fa="fa-smile-o"
					store={store}
					label=":)"
					labelBottom={i18n(lang, '002')}
					className="show-smiles"
					roomOid={roomOid}
					onClick={onSmiles}
				/>
				{(images && images.length) ||
					(audios && audios.length)
						? <Button
							fa="fa-paperclip fa-flip-horizontal"
							store={store}
							label="+"
							labelBottom={i18n(lang, '003')}
							roomOid={roomOid}
							onClick={onAttach}
						/>
						: ''
				}
				<div class="message-typing">
					<textarea
						class={`type-message${
							getClassForTextarea(roomStates[roomOid])
						}`}
						type="text"
						onKeyDown={onKeyDown}
						onInput={onInput}
						onFocus={onFocus}
						placeholder={i18n(lang, '001')}
					></textarea>
				</div>
				{roomState.to != null ||
					roomState.poid
						? (
							<Button
								fa="fa-times"
								store={store}
								label="×"
								labelBottom={i18n(lang, '004')}
								roomState={roomState}
								roomOid={roomOid}
								onClick={onCancel}
							/>
						)
						: (isTyped
							? <Button
								fa="fa-eraser"
								store={store}
								label="<"
								labelBottom={i18n(lang, '005')}
								roomState={roomState}
								roomOid={roomOid}
								onClick={onCancel}
							/>
							: ''
						)
				}
				<Upload
					store={store}
					lang={lang}
				/>
			</div>
		)
	}
}

export default {Send}

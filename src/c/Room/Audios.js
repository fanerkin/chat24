'use strict'

import element from 'virtual-element'

let Audios = {
	name: 'Audios',

	render (c) {
		let {props} = c
		let {oaudios, audios} = props

		let items = oaudios.map(a =>
			<div class="audio">
				<img class={`img${audios.has(a.oid)
						? ' o-green'
						: ''
					}`}
					src="img/music_player.png"
					data-oid={a.oid}
					data-path={a.path}
					alt={a.path}
				/>
				<div style="max-width:0.6rem;overflow:hidden" class="f-xs">
					{a.name}
				</div>
			</div>
		)

		return (
			<div class="audio-adding">
				{items}
			</div>
		)
	}
}

export default {Audios}

'use strict'

import element from 'virtual-element'

let Pictures = {
	name: 'Pictures',

	render (c) {
		let {props} = c
		let {oimages, images} = props

		let items = oimages.map(image =>
			<div class="picture">
				<img class={`img oid_${
						image.oid
					}${
						images.has(image.oid)
							? ' o-green'
							: ''
					}`}
					src={image.path}
					title={image.name}
					alt={image.name}
				/>
			</div>
		)

		return (
			<div class="picture-adding">
				{items}
			</div>
		)
	}
}

export default {Pictures}

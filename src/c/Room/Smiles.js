'use strict'

import element from 'virtual-element'

let Smiles = {
	name: 'Smiles',

	render (c) {
		let {props} = c
		let {smiles} = props

		let children = Object.keys(smiles)
			.map(smile =>
				`<div class="smile"><img class="img" alt="${
					smile
				}" title="${
					smile
				}" src="${
					smiles[smile]
				}"></img></div>`
			)
			.join('')

		return (
			<div class="modal-inner">
				<div class="smile-adding"
					innerHTML={children}
				>
				</div>
			</div>
		)
	}
}

export default {Smiles}

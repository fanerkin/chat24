'use strict'

import element from 'virtual-element'

function onMessage (e, c) {
	let {store, roomOid, message, own} = c.props
	if (message.oid_from === 0) {
		return
	}
	let ta = document.querySelector(`.type-message`)
	if (own) {
		store.dispatch({
			type: 'SET_MESSAGE_MODE',
			mode: 'edit',
			room: roomOid,
			poid: own ? message.oid : null
		})
		ta.value = message.raw
		store.dispatch({
			type: 'SET_TEXT_TYPED',
			room: roomOid,
			isTyped: !!ta.value.length
		})
	} else {
		store.dispatch({
			type: 'SET_MESSAGE_MODE',
			mode: 'appeal',
			room: roomOid
		})
		if (!~ta.value.indexOf(`${message.name_from}`)) {
			ta.value = `${message.name_from}, ${ta.value}`
			store.dispatch({
				type: 'SET_TEXT_TYPED',
				room: roomOid,
				isTyped: !!ta.value.length
			})
		}
	}
}

let Text = {
	name: 'Text',

	render (c) {
		let {store, roomState, isSourceHtml,
			message, lang} = c.props
		let {i18n, escapes} = store.tools

		let isDim = roomState.to != null &&
			roomState.to !== message.oid_from &&
			roomState.to !== message.oid_to

		let html = message.oid_from === 0
			? i18n(lang, message.raw)
			: isSourceHtml
				? message.html
				: null

		return (
			<div class={isDim
					? 'dim'
					: ''
				}
				onClick={onMessage}
			>
				{html
					? <div class="text"
						innerHTML={html}
					>
					</div>
					: message.raw
						.split(/\n/)
						.map(line => escapes.escape(line))
						.map(line =>
							<div class="text"
								innerHTML={line}
							>
							</div>
						)
				}
			</div>
		)
	}
}

export default {Text}

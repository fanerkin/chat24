'use strict'

import element from 'virtual-element'

let Avatar = {
	name: 'Avatar',

	render (c) {
		let {props} = c
		let {message, own} = props

		let imgPath
		let abbr

		if (message.avatar) {
			imgPath = message.avatar
		} else {
			abbr = message.name_from.slice(0, 3)
		}
		return (own
			? <div></div>
			: abbr
				? <div class="avatar">
					<div>
						<span class={`value`}>
							{`${abbr}`}
						</span>
					</div>
				</div>
				: <div class="avatar"
					style={`background:url(${
						imgPath
					}) center no-repeat;background-size:auto 0.5rem;`}
				>
				</div>
		)
	}
}

export default {Avatar}

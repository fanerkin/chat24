'use strict'

import element from 'virtual-element'

function createAudio (file) {
	return `<a class="audio" href="${
			file.path
		}" target="_blank"><img title="${
			file.artist}: ${
			file.title}" src="img/music_player.png"/></a>`
}

function createImage (file) {
	return `<a class="picture" href="${
		file.norm}" target="_blank"><img src="${
		file.min}"/></a>`
}

function onAttachment (e, c) {
	let {store, message} = c.props
	let wrapper = e.target.parentNode
	if (wrapper.className === 'audio') {
		e.preventDefault()
		let ee = new Event('play')
		ee.src = wrapper.href
		let audio = message.audios.filter(audio =>
			~ee.src.indexOf(audio.path))[0]
		store.log(`${audio.artist}\n${audio.title}`)
		document.dispatchEvent(ee)
	} else if (wrapper.className === 'picture') {
		e.preventDefault()
		let src = wrapper.href
		let image = message.images.filter(image =>
			~src.indexOf(image.norm))[0]
		alert({image})
	}
}

let Attachment = {
	name: 'Attachment',

	shouldUpdate (c, nextProps) {
		return c.props.store.tools.shouldUpdate(c, nextProps)
	},

	render (c) {
		let {props} = c
		let {message, isSourceHtml} = props

		return (
			<div class="attachment"
				onClick={onAttachment}
				innerHTML={
					message.images.map(image =>
						createImage(image)).join('') +
					message.audios.map(audio =>
						createAudio(audio)).join('') +
					(isSourceHtml &&
						message.youtube &&
						message.youtube.length
							? message.youtube.map(aa =>
								`<iframe width="212" height="120" src=${
									aa.url
								} frameborder="0" allowfullscreen></iframe>`
							).join('')
							: ''
					)
				}
			>
			</div>
		)
	}
}

export default {Attachment}

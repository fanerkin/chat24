'use strict'

import element from 'virtual-element'
import {Attachment} from './Attachment'
import {Status} from './Status'
import {Text} from './Text'
import {Avatar} from './Avatar'

let Message = {
	name: 'Message',

	afterMount (c, el) {
		let {props} = c
		el.classList.add('animated', 'bounceInUp')
		setTimeout(() => {
			el.classList.remove('animated', 'bounceInUp')
		}, 800)

		if (props.store.getState().isAutoscrollEnabled) {
			document.dispatchEvent(new Event('scrollToBottom'))
		}
	},

	shouldUpdate (c, nextProps) {
		return c.props.store.tools.shouldUpdate(c, nextProps)
	},

	render (c) {
		let {store, roomOid, roomState, isTyping, isSourceHtml,
			messageStack, message, own, lang} = c.props

		return (
			<div class={`message${own
					? ' message-own'
					: ' message-others'
				}`}
			>
				<Avatar
					store={store}
					own={own}
					message={message}
				/>
				<div class="message-main">
					<Status
						store={store}
						own={own}
						isTyping={isTyping}
						roomOid={roomOid}
						roomState={roomState}
						messageStack={messageStack}
						message={messageStack[0]}
					/>
					<div class={`${message.oid_from
							? 'text-user'
							: 'text-system'
						}${message.private
							? ' bg-red'
							: ''
						}`}
					>
						<Attachment
							store={store}
							isSourceHtml={isSourceHtml}
							message={message}
						/>
						<Text
							store={store}
							own={own}
							roomOid={roomOid}
							roomState={roomState}
							isSourceHtml={isSourceHtml}
							message={message}
							lang={lang}
						/>
					</div>
				</div>
			</div>
		)
	}
}

export default {Message}

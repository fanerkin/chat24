'use strict'

import element from 'virtual-element'

function getClassName (own, message, isTyping, roomState) {
	let classNames = {
		'name': true,
		'private-after': message.private && own,
		'private-before': message.private && !own,
		'appeal': message.appeal && !own,
		'typing': isTyping,
		'prompt': message.oid_from === roomState.to
	}
	return Object.keys(classNames)
		.filter(className => classNames[className])
		.join(' ')
}

let Name = {
	name: 'Name',

	shouldUpdate (c, nextProps) {
		return c.props.store.tools.shouldUpdate(c, nextProps)
	},

	render (c) {
		let {props} = c
		let {roomState, message, isTyping, own} = props

		let prefix = own && message.private
			? `${message.name_to} < `
			: ''
		return (
			<span class={getClassName(own, message, isTyping, roomState)}>
				{prefix + message.name_from}
			</span>
		)
	}
}

export default {Name}

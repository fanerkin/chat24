'use strict'

import element from 'virtual-element'

function getLocalTimeString (gmt, offset) {
	return (new Date((gmt - offset) * 1000)).toLocaleTimeString()
}

let Time = {
	name: 'Time',

	shouldUpdate (c, nextProps) {
		return c.props.store.tools.shouldUpdate(c, nextProps)
	},

	render (c) {
		let {props} = c
		let {store, roomState,
			message, messageStack, own} = props
		let time = getLocalTimeString(message.time, store.offset)

		return (own
			? <span class={`value${
					message.oid === roomState.poid
						? ' prompt'
						: ''
					}${messageStack.length > 1
						? ' click'
						: ''
					}`}
			>
				{time}
			</span>
			: <span class={`value${
					messageStack.length > 1
					? ' click'
					: ''
				}`}
			>
				{time}
			</span>
		)
	}
}

export default {Time}

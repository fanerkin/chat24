'use strict'

import element from 'virtual-element'
import {Time} from './Time'
import {Name} from './Name'

function showHistory (o, messageStack) {
	alert(messageStack
		.map(message =>
			['audios', 'images', 'raw']
				.filter(field => message[field].length)
				.reduce((base, field) => {
					base[field] = message[field]
					return base
				}, {})
		)
		.map(JSON.stringify)
		.join(',\n"')
	)
}

function onStatus (e, c) {
	let {store, roomOid, message, messageStack} = c.props
	let o = store.getState()
	let target = e.target

	if (target.classList.contains('value') &&
		target.classList.contains('click')) {
		showHistory(o, messageStack)
	} else if (target.classList.contains('private')) {
		store.dispatch({
			type: 'SET_MESSAGE_MODE',
			mode: 'private',
			room: roomOid,
			to: message.oid_from
		})
	} else if (target.classList.contains('debug')) {
		alert(JSON.stringify(messageStack)
			.split(',"')
			.join(',\n"')
		)
	}
}

let Status = {
	name: 'Status',

	shouldUpdate (c, nextProps) {
		return c.props.store.tools.shouldUpdate(c, nextProps)
	},

	render (c) {
		let {props} = c
		let {store, roomState,
			message, messageStack, isTyping, own} = props

		return (own
			? <div class="status"
				onClick={onStatus}
			>
				<span class="controls debug">
					debug
				</span>
				<Name
					store={store}
					own={own}
					message={message}
					roomState={roomState}
					isTyping={isTyping}
				/>
				<Time
					store={store}
					own={own}
					message={message}
					messageStack={messageStack}
					roomState={roomState}
				/>
			</div>
			: <div class="status"
				onClick={onStatus}
			>
				<Time
					store={store}
					own={own}
					message={message}
					messageStack={messageStack}
					roomState={roomState}
				/>
				<Name
					store={store}
					own={own}
					message={message}
					roomState={roomState}
					isTyping={isTyping}
				/>
				<span class="controls private private-before">
					private
				</span>
				<span class="controls debug">
					debug
				</span>
			</div>
		)
	}
}

export default {Status}

'use strict'

import element from 'virtual-element'
import {Button} from '../Button'

function format (time) {
	let
		minutes,
		seconds

	function fixDigits (n) {
		return (n < 10 ? '0' : '') + n
	}

	minutes = Math.floor(time / 60)
	seconds = Math.floor(time % 60)

	return minutes + ':' + fixDigits(seconds)
}

function onSliderChange (e) {
	let range = e.target
	let ee = new Event('seek')

	ee.time = range.value

	document.dispatchEvent(ee)
	range.blur()
}

function onPlayback (e, c) {
	if (c.props.store.getState().audioPlayer.isPaused) {
		document.dispatchEvent(new Event('play'))
	} else {
		document.dispatchEvent(new Event('pause'))
	}
}

let AudioPlayer = {
	name: 'AudioPlayer',

	shouldUpdate (c, nextProps) {
		return c.props.store.tools.shouldUpdate(c, nextProps)
	},

	render (c) {
		let {props} = c
		let {store, getClassName, isOpen, audioPlayer} = props

		return (
			<div class={getClassName(isOpen, 'audio') +
					(audioPlayer.duration
						? ''
						: ' hide'
					)
				}
			>
				{isOpen
					? (audioPlayer.duration
						? <div class="audio-player menu-inner">
							<div class="info">
								<span class="value">
									{format(audioPlayer.duration)}
								</span>
								<span class="name">
									{format(audioPlayer.position)}
								</span>
							</div>
							<div class="row controls">
								<input
									class="range"
									type="range"
									step="0.01"
									min="0"
									max={audioPlayer.duration}
									value={audioPlayer.position}
									onChange={onSliderChange}
								/>
							</div>
							<div class="row controls">
								{audioPlayer.isPaused
									? <Button
										fa="fa-play"
										store={store}
										label=">"
										onClick={onPlayback}
									/>
									: <Button
										fa="fa-pause"
										store={store}
										label="||"
										onClick={onPlayback}
									/>
								}
							</div>
						</div>
						: '')
					: <i class="fa fa-music menu-audio"></i>
				}
			</div>
		)
	}
}

export default {AudioPlayer}

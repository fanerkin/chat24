'use strict'

import element from 'virtual-element'
import {Button} from '../Button'

// FIXME may fail if new oids > 2
function getUnique (a, b) {
	let h = {}
	let unique
	a.forEach(oid => h[oid] = true)
	b.every(oid => {
		if (oid in h) {
			return true
		}
		unique = oid
		return false
	})

	return unique
}

function onCreateRoom (e, c) {
	let {store} = c.props
	let {API} = store
	let {i18n, log} = store.tools
	let o = store.getState()
	let roomOids = o.rooms.map(room => room.oid)
	let name = prompt(i18n(o.lang, '032'), '')
	if (name && name.length > 2) {
		API.room.create({name})
			// new room has to have {inside: true}
			.then(oo => {
				let newRoomOids = oo.rooms.map(room => room.oid)
				let newRoomOid = getUnique(roomOids, newRoomOids)
				onActivate(newRoomOid, e, c)
			})
			.catch(log)
	} else {
		alert(i18n(o.lang, '106;3;30'))
	}
}

function onJoin (roomOid, e, c) {
	let {store} = c.props
	let {API} = store
	let {log} = store.tools
	API.room.join({room: roomOid})
		.then(() => {
			onActivate(roomOid, e, c)
		})
		.catch(log)
}

function onActivate (roomOid, e, c) {
	let {store} = c.props
	store.dispatch({
		type: 'CHANGE_ROOM',
		room: roomOid
	})
	let ta = document.querySelector(`.type-message`)
	// not personal
	if (ta) {
		ta.value = ''
	}
}

function getRoomList (store, rooms, join) {
	let onClick = join ? onJoin : onActivate
	return rooms.map(room => (
			<div class="list-item"
				onClick={onClick.bind(null, room.oid)}
			>
				<span class="name">
					{room.name}
				</span>
				<span class="value">
					{room.amount}
				</span>
			</div>
		)
	)
}

let RoomList = {
	name: 'RoomList',

	shouldUpdate (c, nextProps) {
		return c.props.store.tools.shouldUpdate(c, nextProps)
	},

	render (c) {
		let {store, getClassName, isOpen, rooms, name, lang} = c.props
		let {i18n} = store.tools
		let outside = getRoomList(
			store,
			rooms.filter(room => !room.inside),
			true
		)
		let inside = getRoomList(
			store,
			rooms.filter(room => room.inside),
			false
		)

		return (name
			? <div class={getClassName(isOpen, 'roomlist')}>
				{isOpen
					? <div class="rooms menu-inner">
						<div class="menu-button">
							<Button
								fa="fa-plus"
								store={store}
								label="+"
								labelBottom={i18n(lang, '016')}
								className="create-room"
								onClick={onCreateRoom}
							/>
						</div>
						<div class="inside">
							{i18n(lang, '027')}:
							<div class="list">
								{inside}
							</div>
						</div>
						<div class="outside">
							{i18n(lang, '028')}:
							<div class="list">
								{outside}
							</div>
						</div>
					</div>
					: <i class="fa fa-sitemap menu-roomlist"></i>
				}
			</div>
			: <div></div>
		)
	}
}

export default {RoomList}

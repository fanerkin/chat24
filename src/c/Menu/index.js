'use strict'

import element from 'virtual-element'
import {Login} from './Login'
import {RoomList} from './RoomList'
import {UserList} from './UserList'
import {AudioPlayer} from './AudioPlayer'
import {Hamburger} from './Hamburger'

function asyncToggleRoomlist (o, store) {
	let {API} = store
	let {log} = store.tools
	clearTimeout(API.timeout)
	API.room.getData({
		mask: o.mask | 2,
		isAbortingPrevious: true,
		once: true
	})
	.then(() => {
		store.dispatch({
			type: 'TOGGLE_MENU',
			item: 'roomlist'
		})
	})
	.catch(log)
}

function onMenuClick (e, c) {
	let {store, menuOpen} = c.props
	let o = store.getState()
	let classList = e.target.classList

	let simpleMenuItem = e.target.className
		.match(/menu-(hamburger|audio|userlist|login)/)
	if (simpleMenuItem) {
		store.dispatch({
			type: 'TOGGLE_MENU',
			item: simpleMenuItem[1]
		})
	} else if (classList.contains('menu-roomlist')) {
		if (menuOpen === 'roomlist') {
			store.dispatch({
				type: 'TOGGLE_MENU',
				item: 'roomlist'
			})
		} else {
			asyncToggleRoomlist(o, store)
		}
	}
}

function getClassName (isOpen, name) {
	return `menu-${name} menu-item${
		isOpen
			? ' open'
			: ''
	}`
}

let names = [
	'audio',
	'userlist',
	'roomlist',
	'hamburger',
	'login'
]

let Menu = {
	name: 'Menu',

	shouldUpdate (c, nextProps) {
		return c.props.store.tools.shouldUpdate(c, nextProps)
	},

	render (c) {
		let {store, menuOpen, audioPlayer,
			name, rooms, users, lang, avatar,
			isSourceHtml, isThemeLight, weather} = c.props

		let children = [
			<AudioPlayer
				store={store}
				isOpen={menuOpen === 'audio'}
				getClassName={getClassName}
				audioPlayer={audioPlayer}
			/>,
			<UserList
				store={store}
				isOpen={menuOpen === 'userlist'}
				getClassName={getClassName}
				name={name}
				users={users}
				lang={lang}
			/>,
			<RoomList
				store={store}
				isOpen={menuOpen === 'roomlist'}
				getClassName={getClassName}
				name={name}
				rooms={rooms}
				lang={lang}
			/>,
			<Hamburger
				store={store}
				isOpen={menuOpen === 'hamburger'}
				getClassName={getClassName}
				name={name}
				lang={lang}
				isSourceHtml={isSourceHtml}
				isThemeLight={isThemeLight}
				weather={weather}
			/>,
			<Login
				store={store}
				isOpen={menuOpen === 'login'}
				getClassName={getClassName}
				className={name && !menuOpen
					? ' avatar'
					: ''
				}
				name={name}
				lang={lang}
				avatar={avatar}
			/>
		]

		return (
			<div class={`menu${
					menuOpen
						? ' open'
						: ''
				}`}
				onClick={onMenuClick}
			>
				{menuOpen
					? children[names.indexOf(menuOpen)]
					: children
				}
			</div>
		)
	}
}

export default {Menu}

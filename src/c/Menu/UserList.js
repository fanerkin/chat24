'use strict'

import element from 'virtual-element'

function onGetPersonal (e, c) {
	let {store} = c.props
	let {API} = store
	API.personal.get()
}

function onPersonal (to, e, c) {
	let {store} = c.props
	let {API} = store
	let {i18n} = store.tools
	let o = store.getState()
	let message = prompt(i18n(o.lang, '033'), '')
	if (message) {
		API.personal.send({
			message,
			personal: 1,
			to
		},
			(value) => alert(value),
			(value) => alert(value)
		)
	}
}

function getUserList (users) {
	return (users.map(user =>
			<div class="list-item"
				onClick={onPersonal.bind(null, user.oid)}
			>
				<span class="name">
					{user.name}
				</span>
			</div>
		)
	)
}

let UserList = {
	name: 'UserList',

	shouldUpdate (c, nextProps) {
		return c.props.store.tools.shouldUpdate(c, nextProps)
	},

	render (c) {
		let {store, getClassName, isOpen, users, name, lang} = c.props
		let {i18n} = store.tools
		let userList = getUserList(users)

		return (name
			? <div class={getClassName(isOpen, 'userlist')}>
				{isOpen
					? <div class="users menu-inner">
						<div class="list list-item"
							onClick={onGetPersonal}
						>
							<span class="value">
								{i18n(lang, '026')}
							</span>
						</div>
						<div class="users">
							{i18n(lang, '021')}:
							<div class="list">
								{userList}
							</div>
						</div>
					</div>
					: <i class="fa fa-users menu-userlist"></i>
				}
			</div>
			: <div></div>
		)
	}
}

export default {UserList}

'use strict'

import element from 'virtual-element'

let Avatars = {
	name: 'Avatars',

	render (c) {
		let {store} = c.props
		let o = store.getState()
		let items = Object.keys(o.defaultAvatars).map(key =>
			<div class="picture">
				<img class={`img oid_${key}`}
					src={`${o.defaultAvatars[key]}`}
					title={key}
					alt={key}/>
			</div>
		)

		return (
			<div class="picture-adding">
				{items}
			</div>
		)
	}
}

export default {Avatars}

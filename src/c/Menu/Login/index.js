'use strict'

import element from 'virtual-element'
import {Avatars} from './Avatars'
import {Pictures} from '../../Room/Pictures'
import {Button} from '../../Button'

function onCreate (e, c) {
	let {store} = c.props
	let {API} = store
	let {i18n} = store.tools
	let o = store.getState()
	let password = prompt(i18n(o.lang, '031'), '')
	if (password) {
		API.pwd.create({password})
	}
}

function onClear (e, c) {
	let {store} = c.props
	let {API} = store
	API.pwd.clear()
}

function onUnlink (e, c) {
	let {store} = c.props
	let {API} = store
	API.user.unlink()
}

function onLogout (e, c) {
	let {store} = c.props
	let {API} = store
	API.user.logout()
}

function onLogin (e, c) {
	let {store} = c.props
	let {API} = store
	let {i18n} = store.tools
	let o = store.getState()
	let nickname = prompt(i18n(o.lang, '030'), '')
	if (nickname && nickname.length > 2) {
		API.user.login({nickname})
	} else {
		alert(i18n(o.lang, '104;3;30'))
	}
}

function onLink (e, c) {
	let {store} = c.props
	let {API} = store
	let {i18n} = store.tools
	let o = store.getState()
	let password = prompt(i18n(o.lang, '031'), '')
	if (password) {
		let nickname = prompt(i18n(o.lang, '030'), '')
		if (nickname) {
			API.user.link({
				password,
				nickname
			})
		} else {
			alert(i18n(o.lang, '104;3;30'))
		}
	}
}

function onAvatar (e, c) {
	let {store} = c.props
	let {API, dom} = store
	let o = store.getState()

	dom.choose({
		c: (
			<div class="modal-inner">
				<Avatars store={store}/>
				<Pictures images={{has () { return false }}}
					oimages={o.images}
				/>
			</div>
		)
	}, e => {
		function onImage (e) {
			let imageOid = +e.target.className
				.replace(/.*oid_(\d+).*/, '$1')
			API.file.toAvatar(imageOid)
			document.querySelector('.close').click()
		}

		let classList = e.target.parentNode.classList
		if (!e.target.classList.contains('img')) {
			return
		} else if (classList.contains('picture')) {
			return onImage(e)
		}
	})
}

let Login = {
	name: 'Login',

	shouldUpdate (c, nextProps) {
		return c.props.store.tools.shouldUpdate(c, nextProps)
	},

	render (c) {
		let {props} = c
		let {store, getClassName, isOpen, name, lang,
			avatar, className} = props
		let {i18n} = store.tools

		return (
			<div class={getClassName(isOpen, 'login') + className}
				style={avatar && !isOpen
					? `background:url(${
							avatar
						}) center no-repeat;background-size:auto 0.5rem;`
					: ''
				}
			>
				{isOpen
					? (name
						? <div class="login menu-inner">
							<div class="name click"
								onClick={onLogin}>
								{name}
							</div>
							<div class="menu-button">
								<Button
									fa="fa-chain-broken"
									store={store}
									label={i18n(lang, '024')}
									labelBottom={i18n(lang, '011')}
									onClick={onUnlink}
								/>
							</div>
							<div class="menu-button">
								<Button
									fa="fa-sign-out"
									store={store}
									label={i18n(lang, '025')}
									labelBottom={i18n(lang, '012')}
									onClick={onLogout}
								/>
							</div>
							<div class="menu-button">
								<Button
									fa="fa-lock"
									store={store}
									label={'"qwerty"'}
									labelBottom={i18n(lang, '013')}
									onClick={onCreate}
								/>
							</div>
							<div class="menu-button">
								<Button
									fa="fa-unlock"
									store={store}
									label="-"
									labelBottom={i18n(lang, '014')}
									onClick={onClear}
								/>
							</div>
							<div class="menu-button">
								<Button
									fa="fa-user-times"
									store={store}
									label="8x"
									labelBottom={i18n(lang, '018')}
									onClick={onLogin}
								/>
							</div>
							<div class="menu-button">
								<Button
									fa="fa-user"
									store={store}
									label="8"
									labelBottom={i18n(lang, '015')}
									onClick={onAvatar}
								/>
							</div>
						</div>
						: <div class="login menu-inner">
							<div>
								{i18n(lang, '039')}
							</div>
							<div class="menu-button">
								<Button
									fa="fa-link"
									store={store}
									label={i18n(lang, '019')}
									labelBottom={i18n(lang, '020')}
									onClick={onLink}
								/>
							</div>
							<div class="menu-button">
								<Button
									fa="fa-sign-in"
									store={store}
									label={i18n(lang, '017')}
									labelBottom={i18n(lang, '018')}
									onClick={onLogin}
								/>
							</div>
						</div>)
					: avatar
						? ''
						: <div class="menu-login">
							{name
								? <span class="value menu-login">
									{`${name.slice(0, 3)}`}
								</span>
								: <i class="fa fa-sign-in menu-login"></i>
							}
						</div>
				}
			</div>
		)
	}
}

export default {Login, onLogin, onLink}

'use strict'

import element from 'virtual-element'

let Weather = {
	name: 'Weather',

	render (c) {
		let {store, weather, lang} = c.props
		let {i18n} = store.tools

		let children = weather.length === 2
			? ['Moscow', 'London'].map((city, i) => {
				let date
				return (
					<div>
						<div>
							{i18n(lang, `w${city}`)}, {
								(date = weather[i].date
									.match(/\d{1,2}:\d{1,2} \w{2}/i))
										? date[0].toUpperCase()
										: ''
								}:
						</div>
						<div>
							{weather[i].temp + '\xB0C'}, {
								i18n(lang, `w${weather[i].code}`)}
						</div>
					</div>
				)
			})
			: ''

		return (
			<div>
				{children}
			</div>
		)
	}
}

export default {Weather}

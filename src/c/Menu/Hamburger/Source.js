'use strict'

import element from 'virtual-element'
import {Button} from '../../Button'

function onToggleSource (e, c) {
	c.props.store.dispatch({
		type: 'TOGGLE_SOURCE'
	})
}

let Source = {
	name: 'Source',

	render (c) {
		let {props} = c
		let {store, isSourceHtml, lang} = props
		let {i18n} = store.tools

		return (
			<div class="menu-button">
				<Button
					fa="fa-code"
					store={store}
					labelTop={
						isSourceHtml
							? 'html'
							: 'raw'
					}
					label="</>"
					labelBottom={i18n(lang, '008')}
					className="toggle-source"
					onClick={onToggleSource}
				/>
			</div>
		)
	}
}

export default {Source}

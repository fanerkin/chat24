'use strict'

import element from 'virtual-element'
import {Button} from '../../Button'

function onToggleTheme (e, c) {
	c.props.store.dispatch({
		type: 'TOGGLE_THEME'
	})
}

let Theme = {
	name: 'Theme',

	render (c) {
		let {props} = c
		let {store, isThemeLight, lang} = props
		let {i18n} = store.tools

		return (
			<div class="menu-button">
				<Button
					fa="fa-adjust"
					store={store}
					labelTop={
						isThemeLight
							? 'light'
							: 'dark'
					}
					label={i18n(lang, '022')}
					labelBottom={i18n(lang, '009')}
					className="toggle-theme"
					onClick={onToggleTheme}
				/>
			</div>
		)
	}
}

export default {Theme}

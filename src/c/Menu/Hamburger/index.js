'use strict'

import element from 'virtual-element'
import {Weather} from './Weather'
import {Source} from './Source'
import {Theme} from './Theme'
import {Lang} from './Lang'
import {Button} from '../../Button'

function onShowLog (e, c) {
	let {store} = c.props
	let o = store.getState()
	let head = store.log.getHead()
	alert(head.join('\n'))
	store.rerender(o)
}

let Hamburger = {
	name: 'Hamburger',

	shouldUpdate (c, nextProps) {
		return c.props.store.tools.shouldUpdate(c, nextProps)
	},

	render (c) {
		let {props} = c
		let {store, getClassName, isOpen, name, lang,
			isSourceHtml, isThemeLight, weather} = props

		let logLength = store.log.getLength()
		return (name
			? <div class={getClassName(isOpen, 'hamburger')}>
				{isOpen
					? <div class="menu-inner">
						<Source
							store={store}
							isSourceHtml={isSourceHtml}
							lang={lang}
						/>
						<Theme
							store={store}
							isThemeLight={isThemeLight}
							lang={lang}
						/>
						<Lang
							store={store}
							lang={lang}
						/>
						<Weather
							store={store}
							weather={weather}
							lang={lang}
						/>
						{logLength
							? <div class="menu-button">
								<Button
									store={store}
									label={`info: ${logLength}`}
									onClick={onShowLog}
								/>
							</div>
							: ''
						}
					</div>
					: <i class="fa fa-bars menu-hamburger"></i>
				}
			</div>
			: <div></div>
		)
	}
}

export default {Hamburger}

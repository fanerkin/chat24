'use strict'

import element from 'virtual-element'
import {Button} from '../../Button'

function onLanguage (e, c) {
	let {store} = c.props
	let o = store.getState()
	store.dispatch({
		type: 'SET_LANG',
		lang: o.lang === 'en'
			? 'ru'
			: 'en'
	})
}

let Lang = {
	name: 'Lang',

	render (c) {
		let {props} = c
		let {store, lang} = props
		let {i18n} = store.tools

		return (
			<div class="menu-button">
				<Button
					fa="fa-language"
					store={store}
					labelTop={lang}
					label={i18n(lang, '023')}
					labelBottom={i18n(lang, '010')}
					className="toggle-language"
					onClick={onLanguage}
				/>
			</div>
		)
	}
}

export default {Lang}

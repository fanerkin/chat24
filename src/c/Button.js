'use strict'

import element from 'virtual-element'

let Button = {
	name: 'Button',

	shouldUpdate (c, nextProps) {
		return c.props.store.tools.shouldUpdate(c, nextProps)
	},

	render (c) {
		let {label, labelTop, labelBottom, image, fa,
			size, type, onClick, className} = c.props

		return (
			<div class={`btn ${className || ''}`}
				size={size || 'medium'}
				type={type || 'primary'}
				onClick={onClick}
			>
				<div class="label-top">{labelTop || ''}</div>
				<div class="label">
					{fa
						? <i class={`fa ${fa}`}></i>
						: image
							? <img
								src={image}
								style="height:0.2rem;width:auto;"
								alt={label || ''}
							></img>
							: (label || '')
					}
				</div>
				<div class="label-bottom">{labelBottom || ''}</div>
			</div>
		)
	}
}

export default {Button}

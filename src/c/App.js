'use strict'

import element from 'virtual-element'
import {Room} from './Room'
import {Personal} from './Room/Personal'
import {Menu} from './Menu'

function getStyle (store, o, activeRoom) {
	if (!store.isMobile) {
		let color = o.isThemeLight
			? 'background:linear-gradient' +
				'(rgba(255,255,255,0.5),rgba(255,255,255,0.5))'
			: 'background:linear-gradient' +
				'(rgba(0,0,0,0.7),rgba(0,0,0,0.7))'
		let image = o.activeRoom !== 'personal' && o.rooms.length
			? `,url(${
					activeRoom.background_path
				}) center no-repeat fixed;background-size:cover;`
			: ''
		return color + image
	}
}

function getChild (store, o, activeRoom) {
	let {i18n} = store.tools
	let isOutsideOfRooms = !o.activeRoom
	let roomOid = activeRoom
		? activeRoom.oid
		: 0

	return o.activeRoom === 'personal'
		? <Personal
			store={store}
			roomState={o.roomStates['personal']}
			isSourceHtml={o.isSourceHtml}
			lang={o.lang}
			oid={o.oid}
		/>
		: activeRoom
			? <Room
				store={store}
				roomState={o.roomStates[roomOid]}
				room={activeRoom}
				isOutsideOfRooms={isOutsideOfRooms}
				isSourceHtml={o.isSourceHtml}
				writers={o.writers}
				lang={o.lang}
				name={o.name}
				oid={o.oid}
				images={o.images}
				audios={o.audios}
				roomStates={o.roomStates}
				isTyped={o.isTyped}
			/>
			: i18n(o.lang, '040')
}

let App = {
	name: 'App',

	afterMount (c) {
		let {store} = c.props
		let html = document.querySelector('html')
		let body = document.body
		document.addEventListener('scroll', () => {
			let o = store.getState()
			// ff + chrome
			let isScrolled =
				body.scrollHeight - body.clientHeight - body.scrollTop +
				html.scrollHeight - html.clientHeight - html.scrollTop < 50
			if (isScrolled) {
				if (!o.isAutoscrollEnabled) {
					store.dispatch({
						type: 'SET_AUTOSCROLL',
						value: true
					})
				}
			} else {
				if (o.isAutoscrollEnabled) {
					store.dispatch({
						type: 'SET_AUTOSCROLL',
						value: false
					})
				}
			}
		}, true)
	},

	afterRender (c) {
		let {o} = c.props
		let classList = document.body.classList
		if (o.isThemeLight ^ !classList.contains('dark')) {
			classList.toggle('dark')
		}
	},

	render (c) {
		let {store, o} = c.props
		// room oids are 1 based
		let activeRoom =
			o.rooms.filter(room => room.oid === ((o.activeRoom | 0) || 1))[0]

		let child = getChild(store, o, activeRoom)
		let style = getStyle(store, o, activeRoom)

		return (
			<div style={style}
				class="app"
			>
				<Menu
					store={store}
					menuOpen={o.menuOpen}
					audioPlayer={o.audioPlayer}
					name={o.name}
					users={o.users}
					rooms={o.rooms}
					lang={o.lang}
					avatar={o.avatar}
					isSourceHtml={o.isSourceHtml}
					isThemeLight={o.isThemeLight}
					weather={o.weather}
				/>
				<div class={`table${
						o.menuOpen
							? ' dim'
							: ''
					}`}
				>
					{child}
				</div>
			</div>
		)
	}
}

export default {App}

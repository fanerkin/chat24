'use strict'

// for testing
import {response} from '../test/response'

import element from 'virtual-element'
import {tree, render} from 'deku'
import xtendM from 'xtend/mutable'

import {store} from './o'
import API from './API'
import tools from './tools'
import {container} from './container'
import {choose} from './dom/choose'

import {App} from './c/App'

let {log, fetch} = tools

xtendM(store, {
	tools,
	API,
	dom: {choose},
	offset: new Date().getTimezoneOffset() * 60,
	xhr: new XMLHttpRequest(),
	isOnline: (() => {
		return !document.location.port ||
			/(8181|5000)/.test(document.location.port)
	})(),
	isMobile: (() => {
		return /(Mobi|Tablet)/.test(navigator.userAgent)
	})(),
	log: (function (a) {
		function log (text) {
			a.unshift(text)
			return a
		}
		log.getHead = function getHead () {
			return a.splice(0, 10)
		}
		log.getLength = function getLength () {
			return a.length
		}
		return log
	})([])
})

let o = store.getState()
let app = tree(<App store={store} o={o}/>)
store.rerender = function rerender (o) {
	app.mount(<App store={this} o={o}/>)
}
if (store.isOnline) {
	firstRequest()
// for testing
} else {
	o.mask = 1
	fetch({
		responseText: JSON.stringify(response)
	})
	// originally comes from /smiles.json
	store.dispatch({
		type: 'FETCH',
		defaultSmiles: {':-*': '/img/smiles/sm25.png'}
	})

	render(app, container)
}

// starts periodic data fetching
function firstRequest () {
	Promise.all([
		API.avatars.getPaths(),
		API.smiles.getPaths(),
		API.room.getData({once: true})
	])
	.then(() => {
		let o = store.getState()
		let m = location.hash.match(/#(\d+|personal)/)
		let room
		if (m && m[1]) {
			room = m[1]
		}
		if (room === 'personal') {
			return API.personal.get()
				.then(() => {
					render(app, container)
					API.room.getData()
				})
				.catch(log)
		} else {
			let roomOid = (room | 0)
			let presentRoom = o.rooms
				.filter(room => room.oid === roomOid)[0]
			if (presentRoom && roomOid !== o.activeRoom) {
				if (!presentRoom.inside) {
					return API.room.join({room: roomOid})
						.then(() => {
							store.dispatch({
								type: 'CHANGE_ROOM',
								room: roomOid
							})
							render(app, container)
							API.room.getData()
						})
				} else {
					store.dispatch({
						type: 'CHANGE_ROOM',
						room: roomOid
					})
				}
			}
			render(app, container)
			API.room.getData()
		}
	})
	.catch(log)
}

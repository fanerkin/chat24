'use strict'

import './dom/audio'
import './dom/alert'
import './dom/arrayFrom'
import './dom/canvasToBlob'
import './dom/event'
import './dom/history'
// uses third party api
import './dom/weather'
import './dom/blinker'
let fs = require('fs')

window.URL = window.URL || window.webkitURL

let body = document.body
let container = document.createElement('div')
container.id = 'hornchat'
container.className = 'hornchat'
body.appendChild(container)

let style = document.createElement('style')
style.type = 'text/css'
let css = fs.readFileSync('tmp/build.css', 'utf8')
// if (!/(mobile|tablet)/i.test(window.navigator.userAgent)) {
// 	css = css.replace(/100px/, '150px')
// }
style.appendChild(document.createTextNode(css))
document.head.appendChild(style)

let html = document.querySelector('html')
document.addEventListener('scrollToBottom', () => {
	// ff + chrome
	html.scrollTop = html.scrollHeight
	body.scrollTop = body.scrollHeight
})

export default {container}

'use strict'

import {store} from './o'
import xtend from 'xtend'
import tools from './tools'

let {ajax, log, fetch, i18n} = tools

function post (data, uri) {
	let o = store.getState()
	return ajax({
		method: 'POST',
		uri,
		data: xtend({last_message: o.lastMessage}, data)
	})
}

function fetchIfDiffer (responseText) {
	return fetch({responseText})
}

function fetchPersonal (responseText) {
	return fetch({responseText, isPersonal: true})
}

function fetchAndContinue (responseText) {
	return fetch({
		responseText,
		next: (isSoonerRequestRequired) => {
			if (isSoonerRequestRequired) {
				console.log(`isSoonerRequestRequired`)
				clearTimeout(API.timeout)
				API.timeout = setTimeout(API.room.getData, 500)
			} else {
				API.timeout = setTimeout(API.room.getData, 2000)
			}
		}
	})
}

let API = {
	timeout: 0,
	room: {
		getData (options = {}) {
			let o = store.getState()
			let {isAbortingPrevious, mask, once} = options
			return ajax({
				isAbortingPrevious,
				xhr: store.xhr,
				method: 'GET',
				uri: '/view.json',
				data: {
					room: o.activeRoom | 0,
					mask: mask || o.mask,
					last_message: o.lastMessage,
					typing: o.isTyping | 0
				}
			})
			.then(fetchAndContinue)
			.catch(value => {
				if (once) {
					log(value)
				} else {
					setTimeout(
						API.room.getData.bind(null, options),
						2000
					)
				}
			})
		},
		create (data) {
			let o = store.getState()
			return post(xtend({
				object: 'room',
				mask: o.mask | 2
			}, data), '/add.json')
				.then(fetchIfDiffer)
				.then(oo => {
					if (!oo.info) {
						alert(i18n(o.lang, '124'))
					}
					return oo
				})
				.catch(log)
		},
		join (data) {
			let o = store.getState()
			return post(xtend({mask: o.mask | 18}, data), '/comein.json')
				.then(fetchIfDiffer)
				.catch(log)
		},
		leave (data) {
			let o = store.getState()
			return post(xtend({mask: o.mask | 2}, data), '/getout.json')
				.then(fetchIfDiffer)
				.catch(log)
		}
	},
	pwd: {
		create (data) {
			let o = store.getState()
			return post(xtend({object: 'password'}, data), '/add.json')
				.then(fetchIfDiffer)
				.then(oo => {
					if (!oo.info) {
						alert(i18n(o.lang, '117'))
					}
					return oo
				})
				.catch(log)
		},
		clear () {
			let o = store.getState()
			return post({object: 'password'}, '/remove.json')
				.then(fetchIfDiffer)
				.then(oo => {
					if (!oo.info) {
						alert(i18n(o.lang, '034'))
					}
					return oo
				})
				.catch(log)
		}
	},
	file: {
		upload (fd, cb) {
			let o = store.getState()
			if (fd) {
				fd.append('last_message', o.lastMessage)
				ajax({
					method: 'POST-formdata',
					uri: '/upload.json',
					data: fd
				})
				// show upload button
				.then(responseText => {
					cb()
					return responseText
				})
				.then(fetchIfDiffer)
				// show upload button then log
				.catch(value => {
					cb()
					log('upload failed, error: ' + value)
				})
			}
		},
		delete (data) {
			let o = store.getState()
			// TODO delete files in one post through api
			return Promise.all(data.actions.map(action =>
				post(action, '/remove.json')
			))
			.then(fetchIfDiffer)
			.then(oo => {
				if (!oo.info) {
					alert(i18n(o.lang, '037'))
				}
				return oo
			})
			.catch(log)
		},
		toAvatar (oid) {
			let o = store.getState()
			return post({object: 'avatar', oid}, '/add.json')
				.then(fetchIfDiffer)
				.then(oo => {
					if (!oo.info) {
						alert(i18n(o.lang, '131'))
					}
					return oo
				})
				.catch(log)
		}
	},
	user: {
		link (data) {
			let o = store.getState()
			return post(xtend({
				object: 'alias',
				mask: o.mask | 2
			}, data), '/add.json')
				.then(fetchIfDiffer)
				.then(oo => {
					if (!oo.info) {
						alert(i18n(o.lang, '035'))
					}
					return oo
				})
				.catch(log)
		},
		unlink () {
			let o = store.getState()
			return post({object: 'alias'}, '/remove.json')
				.then(fetchIfDiffer)
				.then(oo => {
					if (!oo.info) {
						alert(i18n(o.lang, '036'))
					}
					return oo
				})
				.catch(log)
		},
		login (data) {
			let o = store.getState()
			return post(xtend({mask: o.mask | 2}, data), '/login.json')
				.then(fetchIfDiffer)
				.catch(log)
		},
		logout () {
			return post({}, '/logout.json')
				.then(oo => {
					store.dispatch({
						type: 'INIT'
					})
					return oo
				})
				.catch(log)
		}
	},
	personal: {
		send (data, onSent, onFail) {
			let o = store.getState()
			let postData = xtend({mask: o.mask}, data)

			return post(postData, '/send.json')
				.then(onSent, onFail)
				.then(fetchIfDiffer)
				.catch(log)
		},
		get () {
			return ajax({
				isAbortingPrevious: true,
				xhr: store.xhr,
				method: 'GET',
				uri: '/personal.json'
			})
			.then(fetchPersonal)
			.then(() => {
				store.dispatch({
					type: 'CHANGE_ROOM',
					room: 'personal'
				})
			})
			.catch(log)
		},
		delete (oids) {
			let o = store.getState()
			return post({object: 'pmessage', oids}, '/remove.json')
				.then(fetchIfDiffer)
				.then(oo => {
					if (!oo.info) {
						alert(i18n(o.lang, '128'))
					}
					return oo
				})
				.catch(log)
		}
	},
	message: {
		send (data, onSent, onFail) {
			let o = store.getState()
			let postData = xtend({mask: o.mask}, data)

			if (o.roomStates[data.room].to != null) {
				postData.private = 1
				postData.to = o.roomStates[data.room].to
			}
			return post(postData, '/send.json')
				.then(onSent, onFail)
				.then(fetchIfDiffer)
				.catch(log)
		}
	},
	avatars: {
		getPaths () {
			return ajax({
				method: 'GET',
				uri: '/avatars.json'
			})
			.then(responseText => {
				store.dispatch({
					type: 'SET_DEFAULT_AVATARS',
					defaultAvatars: JSON.parse(responseText)
				})
			})
			.catch(log)
		}
	},
	smiles: {
		getPaths () {
			return ajax({
				method: 'GET',
				uri: '/smiles.json'
			})
			.then(responseText => {
				store.dispatch({
					type: 'SET_SMILES',
					smiles: JSON.parse(responseText)
				})
			})
			.catch(log)
		}
	}
}

export default API

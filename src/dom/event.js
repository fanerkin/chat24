'use strict'

// also for phantomjs, where Event is an Object
if (typeof Event !== 'function') {
	Object.defineProperty(window, 'Event', {
		value: function Event (name) {
			let e = document.createEvent('Event')
			e.initEvent(name, true, true)
			return e
		}
	})
}

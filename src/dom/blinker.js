'use strict'

let blinkTitle = (() => {
	let interval
	let title = document.title
	let dots = ['•--', '-•-', '--•', '---']
	let link = document.querySelector('.favicon')
	let horn = document.createElement('link')
	horn.className = 'favicon'
	horn.rel = 'icon'
	horn.type = 'image/png'
	horn.href = 'img/horn.png'
	let links = [link, horn]

	return function setTitle (isBlinking) {
		if (isBlinking) {
			if (!interval) {
				interval = setInterval(() => {
					document.title = dots[0]
					dots.push(dots.shift())
					document.head.removeChild(
						document.querySelector('.favicon')
					)
					links.push(links.shift())
					document.head.appendChild(links[0])
				}, 1000)
			}
		} else {
			clearInterval(interval)
			interval = null
			document.title = title
			document.head.removeChild(
				document.querySelector('.favicon')
			)
			document.head.appendChild(link)
		}
	}
})()

document.addEventListener('message', () => {
	if (document.hidden) {
		blinkTitle(true)
	}
})

document.addEventListener('visibilitychange', () => {
	if (!document.hidden) {
		blinkTitle(false)
	}
})

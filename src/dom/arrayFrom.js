'use strict'

// converts Set, Map, arrayLike, no mapping
if (!Array.from) {
	Object.defineProperty(Array, 'from', {
		value: function from (source) {
			let a = []

			if (source) {
				if (source.length) {
					for (let i = source.length; i--;) {
						a[i] = source[i]
					}
				} else if (source.forEach) {
					source.forEach(v => a.push(v))
				}
			}
			return a
		}
	})
}

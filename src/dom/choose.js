'use strict'

import {element, renderString, tree} from 'deku'
import {modal} from './modal'

function choose (data, cb) {
	let info = modal.firstChild
	info.onclick = cb
	info.innerHTML = `<div class="choose">${renderString(tree(data.c))}</div>`
	modal.className = 'modal pop'
}

export default {choose}

'use strict'

import {store} from '../o'

if (!window.Audio) {
	Object.defineProperty(window, 'Audio', {
		value: function Audio () {
			return {
				play () {},
				pause () {},
				src: '',
				duration: 0,
				currentTime: 0,
				paused: true
			}
		}
	})
}

;(() => {
	let audio = new Audio()
	let prefix = 'https://upload.wikimedia.org/wikipedia/commons'
	document.addEventListener('private', () => {
		if (document.hidden) {
			audio.pause()
			audio.src = `${prefix}/4/46/68448_pinkyfinger_Piano_G.ogg`
			audio.play()
		}
	})
	document.addEventListener('appeal', () => {
		if (document.hidden) {
			audio.pause()
			audio.src = `${prefix}/3/34/A-major.ogg`
			audio.play()
		}
	})
})()

;(() => {
	let audio = new Audio()
	audio.ondurationchange = () => {
		store.dispatch({
			type: 'SET_AUDIO_PLAYER_STATE',
			value: {duration: audio.duration}
		})
	}
	audio.ontimeupdate = () => {
		let o = store.getState()
		let currentTime = Math.floor(audio.currentTime)
		if (currentTime !== o.audioPlayer.position) {
			store.dispatch({
				type: 'SET_AUDIO_PLAYER_STATE',
				value: {position: currentTime}
			})
		}
	}
	audio.onpause = () => {
		store.dispatch({
			type: 'SET_AUDIO_PLAYER_STATE',
			value: {isPaused: audio.paused}
		})
	}
	audio.onplaying = () => {
		store.dispatch({
			type: 'SET_AUDIO_PLAYER_STATE',
			value: {isPaused: audio.paused}
		})
	}
	document.addEventListener('play', e => {
		if (e.src) {
			audio.src = e.src
		}
		audio.play()
	})
	document.addEventListener('pause', () => {
		audio.pause()
	})
	document.addEventListener('seek', e => {
		if (audio.src) {
			audio.currentTime = e.time
		}
	})
})()

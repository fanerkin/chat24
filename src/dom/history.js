'use strict'

import {store} from '../o'
import {API} from '../API'
import {log} from '../tools'

window.onpopstate = e => {
	let o = store.getState()
	let roomOid = e.state && e.state.room
	let presentRoom = o.rooms.filter(room => room.oid === roomOid)[0]
	if (presentRoom) {
		if (presentRoom.inside) {
			store.dispatch({
				type: 'CHANGE_ROOM',
				skipPush: true,
				room: roomOid
			})
		} else {
			API.room.join({room: roomOid})
				.then(() => {
					store.dispatch({
						type: 'CHANGE_ROOM',
						skipPush: true,
						room: roomOid
					})
				})
				.catch(log)
		}
	}
}

'use strict'

import {render, append} from './modal'
import {simpleEscape} from '../tools/escapes'
import {i18n} from '../tools'

function alert (data) {
	if (typeof data === 'string') {
		render(
			`<div class="alert${
				data.red
					? ' co-red'
					: ''
			}"><div class="modal-inner">${
				simpleEscape(data)
					.split('\n')
					.map(line => `<div class="alert-line">${line}</div>`)
					.join('')
			}</div></div>`
		)
	} else if (data.image) {
		render(
			`<img class="i-contain" src="${
				data.image.min
			}">`
		)
		let img = new Image()
		img.src = data.image.norm
		img.className = 'i-contain close'
		img.onload = () => {
			append(img)
		}
	} else if (data.lang) {
		render(
			`<div class="alert${
				data.red
					? ' co-red'
					: ''
			}"><div class="modal-inner">${
				i18n(data.lang, data.message)
			}</div></div>`
		)
	}
}

window.alert = alert

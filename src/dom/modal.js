'use strict'

let modal = document.createElement('div')

function empty () {
	modal.innerHTML = '<div class="info close"><div>'
}

function render (html) {
	modal.innerHTML = `<div class="info close">${
		html
	}<div>`
	modal.className = 'modal pop'
}

function append (el) {
	empty()
	modal.firstChild.appendChild(el)
	modal.className = 'modal pop'
}

modal.id = 'modal'
modal.className = 'modal'
empty()
modal.onclick = e => {
	if (e.target.classList.contains('close')) {
		modal.className = 'modal'
		empty()
	}
}
document.body.appendChild(modal)

export default {modal, empty, render, append}

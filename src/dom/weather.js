'use strict'

import {store} from '../o'

setTimeout(() => {
	if (store.isOnline) {
		window.temp = (function temp () {
			let query = encodeURIComponent(
				'select item.condition from weather.forecast ' +
					'where (woeid="2346910" or woeid="44418") and u="c"'
			)
			let s = document.createElement('script')
			s.src = `https://query.yahooapis.com/v1/public/yql?q=${
					query
				}&format=json&callback=temp`
			document.head.appendChild(s)

			return function temp (res) {
				try {
					let conditions = [0, 1]
						.map(i => res.query.results.channel[i].item.condition)

					store.dispatch({
						type: 'SET_WEATHER',
						weather: conditions
					})
				} catch (e) {
					console.log(e)
				}
			}
		})()
	}
})

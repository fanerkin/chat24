'use strict'

// this is from internets
if (!HTMLCanvasElement.prototype.toBlob) {
	Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
		value: function toBlob (cb, type, quality) {
			let binStr = atob(this.toDataURL(type, quality).split(',')[1])
			let len = binStr.length
			let arr = new Uint8Array(len)

			for (let i = 0; i < len; i++) {
				arr[i] = binStr.charCodeAt(i)
			}

			cb(new Blob([arr], {type: type || 'image/png'}))
		}
	})
}

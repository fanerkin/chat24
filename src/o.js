'use strict'

import {createStore} from 'redux'
import xtend from 'xtend'
import {RoomState} from './RoomState'

let o = {
	oid: 0,
	name: '',
	avatar: '',
	isTyping: false,
	isTyped: false,
	roomStates: {
		personal: new RoomState(),
		0: new RoomState()
	},
	menuOpen: '',
	activeRoom: 1,
	lang: localStorage.getItem('lang') || 'ru',
	isSourceHtml: localStorage.getItem('isSourceHtml') !== 'false',
	isThemeLight: localStorage.getItem('isThemeLight') !== 'false',
	isAutoscrollEnabled: true,
	lastMessage: 0,
	users: [],
	writers: [],
	rooms: [],
	images: [],
	audios: [],
	videos: [],
	archives: [],
	personalMessages: [],
	hashImage: 0,
	hashAudio: 0,
	hashVideo: 0,
	hashArchive: 0,
	hashPersonal: 0,
	audioPlayer: {
		duration: 0,
		position: 0,
		isPaused: false
	},
	// what to add to response
	// 1 - users, 2 - rooms, 4 - images, 8 - audios
	// 16 - add all messages in a room
	// 32 - videos, 64 - archives
	mask: 3,
	weather: [{
		code: '',
		date: '',
		temp: '',
		text: ''
	}],
	defaultAvatars: {},
	defaultSmiles: {}
}

function cloneSet (set) {
	let s = new Set()
	set.forEach(i => s.add(i))
	return s
}

function updateRoomState (state, roomOid, action) {
	return xtend(state, {
		roomStates: xtend(
			state.roomStates, {
				[roomOid]: xtend(
					state.roomStates[roomOid],
					action
				)
			}
		)
	})
}

function messageMode (action) {
	switch (action.mode) {
	case 'edit':
		return {
			poid: action.poid,
			mode: action.mode
		}
	case 'private':
		return {
			poid: null,
			to: action.to,
			mode: action.mode
		}
	case 'appeal':
		return {
			to: null,
			mode: action.mode
		}
	case 'public':
		return {
			poid: null,
			to: null,
			mode: action.mode
		}
	case 'empty':
		return {
			poid: null,
			imageOids: new Set(),
			audioOids: new Set(),
			mode: action.mode
		}
	case 'public + empty':
		return {
			poid: null,
			to: null,
			imageOids: new Set(),
			audioOids: new Set(),
			mode: action.mode
		}
	default:
		return {}
	}
}

function attachment (state, action) {
	let oids = action.item + 'Oids'
	let newSet = cloneSet(state.roomStates[action.room][oids])
	newSet[action.operation](action.oid)
	return {
		[oids]: newSet
	}
}

/*eslint complexity: 0*/
function update (state = o, action) {
	console.log(action)
	switch (action.type) {
	case 'FETCH':
		return xtend(state, action)
	case 'FETCH_PERSONAL':
		return updateRoomState(state, 'personal', action.value)
	case 'SET_AUTOSCROLL':
		return xtend(state, {isAutoscrollEnabled: action.value})
	case 'TOGGLE_SOURCE':
		localStorage.setItem('isSourceHtml', String(!state.isSourceHtml))
		return xtend(state, {isSourceHtml: !state.isSourceHtml})
	case 'TOGGLE_THEME':
		localStorage.setItem('isThemeLight', String(!state.isThemeLight))
		return xtend(state, {isThemeLight: !state.isThemeLight})
	case 'CHANGE_ROOM':
		if (!action.skipPush) {
			history.pushState(
				{room: action.room},
				'',
				`#${action.room}`
			)
		}
		return xtend(
			updateRoomState(state, action.room,
				messageMode({mode: 'empty'})
			),
			{activeRoom: action.room},
			{isTyped: false}
		)
	case 'SET_MESSAGE_MODE':
		return updateRoomState(state, action.room,
			messageMode(action)
		)
	case 'SET_TYPING':
		return xtend(state, {isTyping: action.value})
	case 'SET_TEXT_TYPED':
		return action.isTyped !== state.isTyped
			? xtend(state, {isTyped: action.isTyped})
			: state
	case 'ATTACHMENT':
		return updateRoomState(state, action.room,
			attachment(state, action)
		)
	case 'TOGGLE_MENU':
		document.body.classList.toggle('overflow-hidden')
		setTimeout(() => {
			document.dispatchEvent(new Event('scrollToBottom'))
		}, 300)
		return xtend(state, {
			menuOpen: state.menuOpen === action.item
				? ''
				: action.item
		})
	case 'SET_AUDIO_PLAYER_STATE':
		return xtend(state, {
			audioPlayer: xtend(state.audioPlayer, action.value)
		})
	case 'SET_LANG':
		localStorage.setItem('lang', action.lang)
		return xtend(state, action)
	case 'SET_WEATHER':
		return xtend(state, action)
	case 'SET_DEFAULT_AVATARS':
		return xtend(state, action)
	case 'SET_SMILES':
		return xtend(state, {defaultSmiles: action.smiles})
	case 'INIT':
		return xtend(state, o)
	default:
		return state
	}
}

let store = createStore(update, o)
/*eslint no-unused-vars: 0*/
let unsubscribe = store.subscribe(() => {
	let state = store.getState()
	store.rerender(state)
})
export default {store}

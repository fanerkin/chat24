'use strict'

function RoomState (state = {
	messageStacks: [],
	to: null,
	poid: null,
	mode: 'public + empty',
	imageOids: new Set(),
	audioOids: new Set()
}, action = Function.prototype) {
	return action(state) || state
}

export default {RoomState}

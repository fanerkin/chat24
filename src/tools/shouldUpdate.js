'use strict'

function shouldUpdate (c, nextProps) {
	let {props} = c
	return Object.keys(props)
		.some(prop =>
			// deku issue #234, always empty array
			prop !== 'children' &&
			props[prop] !== nextProps[prop]
		)
}

export default {shouldUpdate}

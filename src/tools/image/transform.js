'use strict'

/*eslint complexity: 0*/
function fixOrientation (data) {
	let {ctx, width, height, orientation} = data
	switch (orientation) {
	case 2:
		// horizontal flip
		ctx.transform(-1, 0, 0, 1, width, 0)
		break
	case 3:
		// 180° rotate left
		ctx.transform(-1, 0, 0, -1, width, height)
		break
	case 4:
		// vertical flip
		ctx.transform(1, 0, 0, -1, 0, height)
		break
	case 5:
		// vertical flip + 90° rotate right
		ctx.transform(0, 1, 1, 0, 0, 0)
		break
	case 6:
		// 90° rotate right
		ctx.transform(0, 1, -1, 0, height, 0)
		break
	case 7:
		// horizontal flip + 90° rotate right
		ctx.transform(0, -1, -1, 0, height, width)
		break
	case 8:
		// 90° rotate left
		ctx.transform(0, -1, 1, 0, 0, width)
		break
	}
}

export default {fixOrientation}

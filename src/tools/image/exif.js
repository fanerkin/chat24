'use strict'

function findEXIF (file) {
	let dataView = new DataView(file)

	if (dataView.getUint16(0) !== 0xFFD8) {
		// not JPEG
		return 1
	}

	for (let offset = 2, length = file.byteLength;
			offset < length;
			offset += 2 + dataView.getUint16(offset + 2)
	) {
		if (dataView.getUint8(offset) !== 0xFF) {
			// bad marker
			return 2
		}

		// APP1
		if (dataView.getUint8(offset + 1) === 0xE1) {
			if (dataView.getUint32(offset + 4) !== 0x45786966) {
				// No "Exif" in APP1
				return 3
			}
			return findIDF0(dataView, offset + 10)
		}
	}
}

function findIDF0 (dataView, offsetTIFF) {
	let littleEndian

	if (dataView.getUint16(offsetTIFF) === 0x4949) {
		littleEndian = true
	} else if (dataView.getUint16(offsetTIFF) === 0x4D4D) {
		littleEndian = false
	} else {
		// No "II" or "MM" in TIFF data
		return 4
	}

	if (dataView.getUint16(offsetTIFF + 2, littleEndian) !== 0x002A) {
		// No 42 in TIFF data
		return 5
	}

	let offsetIDF0 = dataView.getUint32(offsetTIFF + 4, littleEndian)

	if (offsetIDF0 < 0x00000008) {
		// First offset is less than 8 in TIFF data
		return 6
	}

	return findOrientation(dataView, offsetTIFF + offsetIDF0, littleEndian)
}

function findOrientation (dataView, start, littleEndian) {
	let entriesTotal = dataView.getUint16(start, littleEndian)
	let entryOffset = start + 2

	for (let i = entriesTotal; i--; entryOffset += 12) {
		if (dataView.getUint16(entryOffset, littleEndian) === 0x0112) {
			return {
				Orientation: dataView.getUint16(entryOffset + 8, littleEndian)
			}
		}
	}
	// No Orientation
	return 7
}

// runs in 0-1 ms, no web worker required
export default {findEXIF}

'use strict'

import {findEXIF} from './exif'
import {fixOrientation} from './transform'

let objectURL
let exif

function readAsArrayBuffer (blob, cb) {
	var reader = new FileReader()
	reader.onloadend = e => {
		cb(e.target.result)
	}
	reader.readAsArrayBuffer(blob)
}

function convert (file, cb) {
	let isJPEG = /\/jpeg/.test(file.type)
	let img = new Image()
	objectURL = URL.createObjectURL(file)
	img.onload = () => {
		let orientation = exif.Orientation
		let canvas = document.createElement('canvas')
		let ratio = img.width / img.height
		let	width
		let	height

		if (img.width > 900 || img.height > 600) {
			if (ratio < 1.5) {
				width = 900 * ratio * 2 / 3
				height = 600
			} else {
				width = 900
				height = 600 * 3 / ratio / 2
			}
		} else {
			width = img.width
			height = img.height
		}
		if (orientation > 4) {
			canvas.width = height
			canvas.height = width
		} else {
			canvas.width = width
			canvas.height = height
		}

		let ctx = canvas.getContext('2d')
		fixOrientation({
			ctx,
			width,
			height,
			orientation
		})
		ctx.drawImage(img, 0, 0, width, height)
		canvas.toBlob(blob => {
			URL.revokeObjectURL(objectURL)
			cb(blob)
		}, 'image/jpeg', 0.9)
	}
	if (isJPEG) {
		readAsArrayBuffer(file, arrayBuffer => {
			exif = findEXIF(arrayBuffer)
			img.src = objectURL
		})
	} else {
		exif = {}
		img.src = objectURL
	}
}

export default {convert}

'use strict'

import {store} from '../o'

function log (value) {
	let text = value != null && typeof value === 'object'
		? JSON.stringify(value)
		: String(value)
	console.log(text)
	store.log(text)
	return value
}

export default {log}

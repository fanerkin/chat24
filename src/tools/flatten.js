'use strict'

function flatten (a) {
	let aa = []
	for (let i = 0, l = a.length; i < l; i++) {
		if (Array.isArray(a[i])) {
			aa = aa.concat(flatten(a[i]))
		} else {
			aa.push(a[i])
		}
	}
	return aa
}

export default {flatten}

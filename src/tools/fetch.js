'use strict'

import {store} from '../o'
import {RoomState} from '../RoomState'
import xtend from 'xtend'
import {flatten} from './flatten'
import {deepEqual} from './deepEqual'

function showInfo (o, oo) {
	if (oo.error) {
		alert({lang: o.lang, message: oo.error, red: true})
		throw new Error(`from server: ${oo.error}`)
	}

	if (oo.info) {
		alert({lang: o.lang, message: oo.info})
	}
}

function fetchPersonal (oo, roomStates) {
	if (oo.messages) {
		store.dispatch({
			type: 'FETCH_PERSONAL',
			value: {messageStacks: oo.messages.map(message => [message])}
		})
	}
}

function fetchRooms (o, oo, action, roomStates) {
	if (oo.rooms) {
		action.mask = o.mask & ~2

		let empty = oo.rooms
			.filter(room => !roomStates[room.oid])
		// initializes new rooms not in state
		if (empty.length) {
			empty.forEach(room => roomStates[room.oid] = new RoomState())
			action.roomStates = roomStates
		}

		let roomsInsideOids = oo.rooms
			.filter(room => room.inside)
			.map(room => room.oid)

		if (!roomsInsideOids.length) {
			if (o.activeRoom) {
				store.dispatch({
					type: 'CHANGE_ROOM',
					room: 0
				})
			}
		} else if (
			o.activeRoom !== 'personal' &&
			!~roomsInsideOids.indexOf(o.activeRoom)
		) {
			store.dispatch({
				type: 'CHANGE_ROOM',
				room: Math.min.apply(Math, roomsInsideOids)
			})
		}
	}
}

function fetchMessages (o, oo, action, roomStates) {
	let hasPrivate
	let hasAppeal

	oo.messages.forEach(message => {
		let roomOid = message.room
		if (!roomStates[roomOid]) {
			roomStates[roomOid] = new RoomState()
			action.roomStates = roomStates
		}
		let {messageStacks} = roomStates[roomOid]
		let isDuplicate = flatten(messageStacks)
			.some(msg => msg.oid === message.oid)
		if (isDuplicate) {
			return
		}

		let i = 0
		let isReplacing = messageStacks.some(stack => {
			if (stack[0].oid === message.poid) {
				return true
			}
			i++
		})
		roomStates = xtend(roomStates)
		if (isReplacing) {
			messageStacks[i] = [message, ...messageStacks[i]]
		} else {
			messageStacks.push([message])
		}
		roomStates[roomOid].messageStacks = messageStacks.concat()
		action.roomStates = roomStates

		if (!hasPrivate &&
			message.private &&
			message.oid_from !== o.oid) {
			hasPrivate = true
		}
		if (!hasAppeal &&
			message.appeal &&
			message.oid_from !== o.oid) {
			hasAppeal = true
		}
		document.dispatchEvent(new Event('message'))
	})

	if (hasPrivate) {
		document.dispatchEvent(new Event('private'))
	} else if (hasAppeal) {
		document.dispatchEvent(new Event('appeal'))
	}
}

function fetchHashes (o, oo, action) {
	;[
		[4, 'hash_image', 'hashImage'],
		[8, 'hash_audio', 'hashAudio'],
		[32, 'hash_video', 'hashVideo'],
		[64, 'hash_archive', 'hashArchive']
	].forEach(([bit, hash_name, hashName]) => {
		if (oo[hash_name] !== o[hashName]) {
			action.mask = (action.mask || o.mask) | bit
			action[hashName] = oo[hash_name]
		}
	})
}

function fetchItems (o, oo, action) {
	;[
		[4, 'images'],
		[8, 'audios'],
		[32, 'videos'],
		[64, 'archives']
	].forEach(([bit, items]) => {
		if (oo[items] != null &&
			!deepEqual(o[items], oo[items])
		) {
			action[items] = oo[items]
			if (o.mask & bit) {
				action.mask = (action.mask || o.mask) & ~bit
			}
		}
	})
}

function fetch ({responseText, isPersonal, next}) {
	let action = {type: 'FETCH'}
	let o = store.getState()
	let oo = JSON.parse(responseText)

	showInfo(o, oo)

	let roomStates = xtend(o.roomStates)

	if (isPersonal) {
		fetchPersonal(oo, action, roomStates)
		return oo
	}

	;['oid', 'name', 'writers', 'users', 'rooms', 'avatar']
		.filter(key => oo[key] != null)
		.filter(key => !deepEqual(o[key], oo[key]))
		.forEach(key => action[key] = oo[key])

	if (oo.last_message != null &&
		oo.last_message !== o.lastMessage) {
		action.lastMessage = oo.last_message
	}

	fetchRooms(o, oo, action, roomStates)
	fetchMessages(o, oo, action, roomStates)
	fetchHashes(o, oo, action)
	fetchItems(o, oo, action)

	let isSoonerRequestRequired = o.mask !== 1

	if (Object.keys(action).length > 1) {
		store.dispatch(action)
	}

	if (next) {
		next(isSoonerRequestRequired)
	}

	return oo
}

export default {fetch, showInfo}

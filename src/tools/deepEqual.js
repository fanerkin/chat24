'use strict'

// stripped down 'deep-equal'
function deepEqual (a, b) {
	var i
	var key
	if (a == null || b == null) {
		return false
	}
	if (typeof a !== 'object' || typeof b !== 'object') {
		return a === b
	} else if (a instanceof Date && b instanceof Date) {
		return a.getTime() === b.getTime()
	} else if (a.prototype !== b.prototype) {
		return false
	}
	try {
		var ka = Object.keys(a)
		var kb = Object.keys(b)
	} catch (e) {
		return false
	}
	if (ka.length !== kb.length) {
		return false
	}
	ka.sort()
	kb.sort()
	for (i = ka.length; i--;) {
		if (ka[i] !== kb[i]) {
			return false
		}
	}
	for (i = ka.length; i--;) {
		key = ka[i]
		if (!deepEqual(a[key], b[key])) {
			return false
		}
	}
	return true
}

export default {deepEqual}

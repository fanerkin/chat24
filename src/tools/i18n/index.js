'use strict'

let fs = require('fs')
let map = {
	en: JSON.parse(fs.readFileSync('src/tools/i18n/en.json', 'utf8')),
	ru: JSON.parse(fs.readFileSync('src/tools/i18n/ru.json', 'utf8'))
}

function i18n (lang, message) {
	if (message[0] === 'w') {
		return map[lang].weather[message.slice(1)]
	}
	let parts = message.split(';')
	let code = parts[0].slice(0, 3)
	let substs = parts.slice(1)
	let stub = map[lang][code]
	substs.forEach(subst => {
		stub = stub.replace('%s', subst)
	})
	return stub
}

export default {i18n}

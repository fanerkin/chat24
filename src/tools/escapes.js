'use strict'

import {store} from '../o'
import xtend from 'xtend'

let o = store.getState()
let oldSmiles = o.defaultSmiles
let forEscaping = {}
let reSimple = /[<>"'&\/]/g
let strRe = `[<>"'&\\/]|https?:\\/\\/\\S+`
let strReSmiles
let re
let defaultRules = {
	'<': '&lt;',
	'>': '&gt;',
	'"': '&quot;',
	"'": '&#39;',
	'&': '&amp;',
	'/': '&#47;'
}
let rules

function init (smiles) {
	Object.keys(smiles).forEach(text => {
		forEscaping[text] = `<span class="smile"><img alt="${
				text
			}" title="${
				text
			}" src="${
				smiles[text]
			}"/></span>`
	})
	strReSmiles = Object.keys(smiles)
		.map(escapeRe)
		.join('|')
	re = new RegExp((strReSmiles ? strReSmiles + '|' : '') + strRe, 'g')
	rules = xtend(defaultRules, forEscaping)
}

function escapeRe (text) {
	return text.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&')
}

function escape (text) {
	let o = store.getState()
	if (oldSmiles !== o.defaultSmiles) {
		oldSmiles = o.defaultSmiles
		init(oldSmiles)
	}
	return text.replace(re, (c) =>
			rules[c] || `<a href="${c}" target="_blank">${c}</a>`
		)
}

function simpleEscape (text) {
	return text.replace(reSimple, (c) =>
			defaultRules[c] || c
		)
}

init(oldSmiles)

export default {escape, simpleEscape}

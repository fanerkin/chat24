'use strict'

function createUrlEncoded (data) {
	return Object.keys(data || {})
		.reduce((base, key) => {
			base.push(`${encodeURIComponent(key)}=${
				encodeURIComponent(data[key])}`)
			return base
		}, [])
		.join('&')
		.replace(/%20/g, '+')
}

function prepare (xhr, uri, verb,
			isPost, isFormData, data, isAbortingPrevious) {
	let prevMethod = xhr && xhr.method

	if (prevMethod &&
		xhr.readyState < 4 &&
		!isPost &&
		!isAbortingPrevious) {
		// a request in progress, skipping if not POST
		return null
	}

	if (!xhr || prevMethod === 'POST') {
		xhr = new XMLHttpRequest()
	}

	// GET, HEAD
	if (!isPost) {
		uri = `${uri}?${createUrlEncoded(data)}`
	}

	xhr.open(verb, uri, true)
	xhr.method = verb
	if (isPost && !isFormData) {
		xhr.setRequestHeader(
			'content-type',
			'application/x-www-form-urlencoded'
		)
	}

	return xhr
}

function handle (xhr, data, isFormData, isPost, resolve, reject) {
	let timeout = setTimeout(() => {
		xhr.method = null
		xhr.abort()
	}, isFormData ? 30000 : 20000)
	xhr.onreadystatechange = () => {
		if (xhr.readyState === 4) {
			clearTimeout(timeout)
			if (xhr.status < 300) {
				resolve(xhr.responseText)
			} else {
				reject('AJAX: status=' + xhr.status)
			}
		}
	}
	xhr.send(isPost && data && !isFormData
		? createUrlEncoded(data)
		: data
	)
}

// TODO consider using/polyfilling 'window.fetch'
function ajax (options) {
	return new Promise((resolve, reject) => {
		let {method, uri, data, xhr, isAbortingPrevious} = options

		let m = method.split('-')
		let verb = m[0]
		let isPost = verb === 'POST'
		let isFormData = !!m[1]

		xhr = prepare(xhr, uri, verb,
			isPost, isFormData, data, isAbortingPrevious)
		if (xhr) {
			handle(xhr, data, isFormData, isPost, resolve, reject)
		}
	})
}

// TODO remove, for testing
window.ajax = ajax

export default {ajax}

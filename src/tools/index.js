'use strict'

import escapes from './escapes'
import image from './image'

import {ajax} from './ajax'
import {i18n} from './i18n'
import {log} from './log'
import {flatten} from './flatten'
import {deepEqual} from './deepEqual'

import {fetch} from './fetch'
import {shouldUpdate} from './shouldUpdate'

export default {
	flatten, deepEqual,
	escapes, image,
	ajax, i18n, log,
	fetch, shouldUpdate
}

export PATH := ./node_modules/.bin:${PATH}

bin = node_modules/.bin
src = $(shell find src -name '*.js')
test = $(shell find test -name '*.js')
scripts = $(shell find scripts -name '*.js')

create_ssl_pair:
	@sh ./scripts/create_ssl_pair.sh
PHONY: create_ssl_pair

browserify: node_modules $(src) | build_css
	@$(bin)/browserify -t babelify -t brfs ./src/index.js > dist/js/index.js
.PHONY: browserify

build_css: $(src)
	@cat src/css/*.css > tmp/concat.css
	@make postcss
.PHONY: build_css

postcss:
	@$(bin)/postcss \
		--use autoprefixer \
			--autoprefixer.browsers "last 2 versions" \
		--use cssnano \
		--output tmp/build.css tmp/concat.css
.PHONY: postcss

clean:
	@-rm -rf dist
.PHONY: clean

dist: | clean stub browserify uglify
.PHONY: dist

fresh: | clean
	@-rm -rf node_modules
.PHONY: fresh

node_modules: package.json
	@npm install

stub:
	@-mkdir log
	@-mkdir tmp
	@-mkdir dist
	@-cp -a ast/* dist
.PHONY: stub

mochify: node_modules $(src) $(test) | build_css
	@$(bin)/mochify --transform babelify --transform brfs --reporter spec test/index.js
.PHONY: mochify

watch_test:
	@$(bin)/mochify --watch --transform babelify --transform brfs --reporter spec test/index.js
.PHONY: watch_test

watch_js:
	@$(bin)/watchify --transform babelify --transform brfs src/index.js -o dist/js/index.js -v
.PHONY: watch_js

watch_min_js:
	@-while true;do inotifywait -q -e close_write dist/js/index.js;make uglify;done
.PHONY: watch_min_js

watch_css:
	@$(bin)/catw src/css/*.css -o tmp/concat.css -v
.PHONY: watch_css

watch_concat_css:
	@-while true;do inotifywait -q -e close_write tmp/concat.css;make postcss;done
.PHONY: watch_min_js

watch_all: | watch_css watch_concat_css watch_js watch_min_js watch_test serve_test
.PHONY: watch_all

watch: | stub
	@make -j watch_all
.PHONY: watch

test: | mochify
.PHONY: test

uglify: node_modules dist/js/index.js
	@$(bin)/uglifyjs dist/js/index.js -mc > dist/js/index.min.js 2>/dev/null;echo "minified"
.PHONY: uglify

lint:
	@$(bin)/eslint $(src) $(test) $(scripts)
.PHONY: lint

log:
	@cat log/info.log | $(bin)/bunyan
.PHONY: log

deploy:
	@sh scripts/deploy.sh
.PHONY: deploy

serve:
	@node scripts/proxy.js
.PHONY: serve

serve_test:
	@node scripts/server_test.js
.PHONY: serve_test

restart:
	@sh scripts/restart.sh
.PHONY: restart

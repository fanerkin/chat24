'use strict'

import assert from 'assert'
import sinon from 'sinon'
import {response, nextMessage} from './response'
import {Event} from './Event'

let o = response
let mm = o.messages[0]

function toggleTextSource (done) {
	let menu = document.body.querySelector('.menu-hamburger')
	menu.dispatchEvent(new Event('click'))

	setTimeout(() => {
		let toggle = document.body.querySelector('.toggle-source')
		toggle.dispatchEvent(new Event('click'))
		let menu = document.body.querySelector('.menu-hamburger')
		menu.dispatchEvent(new Event('click'))
		done()
	}, delay)
}

describe('message', () => {
	describe('raw', () => {
		before(toggleTextSource)
		after(toggleTextSource)
		it('should escape', (done) => {
			let ta = document.body.querySelector('.type-message')
			let server = sinon.fakeServer.create()
			let raw = '><&amp;&lt;&gt;'

			mm.raw = raw
			server.respondWith('POST', '/send.json', nextMessage())

			ta.value = raw
			ta.dispatchEvent(new Event('ctrlEnter'))

			setTimeout(() => {
				server.respond()
				setTimeout(() => {
					let msgs = document.body.querySelectorAll('.message .text')
					assert.equal(msgs.length, mm.oid)
					assert.equal(
						msgs[msgs.length - 1].innerHTML,
						raw.replace(/[&<>]/g, c => ({
							'<': '&lt;',
							'>': '&gt;',
							'&': '&amp;'
						}[c]))
					)
					done()
				}, delay)
			})
		})
		it('should turn http(s) uris to links', (done) => {
			let ta = document.body.querySelector('.type-message')
			let server = sinon.fakeServer.create()
			let uri = 'https://command.com'
			let raw = `z ${uri} z`

			mm.raw = raw
			server.respondWith('POST', '/send.json', nextMessage())

			ta.value = raw
			ta.dispatchEvent(new Event('ctrlEnter'))

			setTimeout(() => {
				server.respond()
				setTimeout(() => {
					let msgs = document.body.querySelectorAll('.message .text')
					assert.equal(msgs.length, mm.oid)
					assert.equal(
						msgs[msgs.length - 1].innerHTML,
						`z <a href="${uri}" target="_blank">${uri}</a> z`
					)
					done()
				}, delay)
			})
		})
		it('should convert smiles to images', (done) => {
			let ta = document.body.querySelector('.type-message')
			let server = sinon.fakeServer.create()
			let raw = ':-*'

			mm.raw = raw
			server.respondWith('POST', '/send.json', nextMessage())

			ta.value = raw
			ta.dispatchEvent(new Event('ctrlEnter'))

			setTimeout(() => {
				server.respond()
				setTimeout(() => {
					let msgs = document.body.querySelectorAll('.message .text')
					assert.equal(msgs.length, mm.oid)
					assert.equal(
						msgs[msgs.length - 1].innerHTML,
'<span class="smile"><img alt=":-*" title=":-*" ' +
'src="/img/smiles/sm25.png"></span>'
					)
					done()
				}, delay)
			})
		})
	})
	describe('html', () => {
		it('should paste as is', (done) => {
			let ta = document.body.querySelector('.type-message')
			let server = sinon.fakeServer.create()
			let raw = 'zzzzzzz'
			let html = '&amp;&lt;&gt;'

			mm.raw = raw
			mm.html = html
			server.respondWith('POST', '/send.json', nextMessage())

			ta.value = raw
			ta.dispatchEvent(new Event('ctrlEnter'))

			setTimeout(() => {
				server.respond()
				setTimeout(() => {
					let msgs = document.body.querySelectorAll('.message .text')
					assert.equal(msgs.length, mm.oid)
					assert.equal(
						msgs[msgs.length - 1].innerHTML,
						html
					)
					done()
				}, delay)
			})
		})
	})
	describe('attachment', () => {
		afterEach(() => {
			mm.audios = []
			mm.images = []
		})
		it('should attach image', (done) => {
			let ta = document.body.querySelector('.type-message')
			let server = sinon.fakeServer.create()
			let raw = 'baab'

			mm.html = mm.raw = raw
			mm.images = [{
				min: '/m123.jpg',
				norm: '/n123.jpg'
			}]
			server.respondWith('POST', '/send.json', nextMessage())

			ta.value = raw
			ta.dispatchEvent(new Event('ctrlEnter'))

			setTimeout(() => {
				server.respond()
				setTimeout(() => {
					let msgs = document.body.querySelectorAll(
						'.message .attachment'
					)
					assert.equal(msgs.length, mm.oid)
					assert.equal(
						msgs[msgs.length - 1].innerHTML,
'<a class="picture" href="/n123.jpg" target="_blank"><img src="/m123.jpg"></a>'
					)
					done()
				}, delay)
			})
		})
		it('should attach audio', (done) => {
			let ta = document.body.querySelector('.type-message')
			let server = sinon.fakeServer.create()
			let raw = 'baab'

			mm.html = mm.raw = raw
			mm.audios = [{
				title: 'ttt',
				artist: 'aaa',
				path: '/a123.mp3'
			}]
			server.respondWith('POST', '/send.json', nextMessage())

			ta.value = raw
			ta.dispatchEvent(new Event('ctrlEnter'))

			setTimeout(() => {
				server.respond()
				setTimeout(() => {
					let msgs = document.body.querySelectorAll(
						'.message .attachment'
					)
					assert.equal(msgs.length, mm.oid)
					assert.equal(
						msgs[msgs.length - 1].innerHTML,
						`<a class="audio" href="${
							mm.audios[0].path
						}" target="_blank"><img title="${
							mm.audios[0].artist
						}: ${
							mm.audios[0].title
						}" src="img/music_player.png"></a>`
					)
					done()
				}, delay)
			})
		})
	})
})

'use strict'

import assert from 'assert'
import sinon from 'sinon'
import {response} from './response'
import {Event} from './Event'

let o = response
let oldMessages

function toggleMenu (item, done) {
	let menu = document.body.querySelector('.menu-' + item)
	menu.dispatchEvent(new Event('click'))
	setTimeout(done, delay)
}

describe('menu', () => {
	before(() => {
		oldMessages = o.messages
		o.messages = []
	})
	after(() => {
		o.messages = oldMessages
	})
	describe('login name', () => {
		beforeEach(toggleMenu.bind(null, 'login'))
		afterEach(toggleMenu.bind(null, 'login'))
		it('should update name', (done) => {
			setTimeout(() => {
				let nameLink = document.body.querySelector('.login .name')
				let server = sinon.fakeServer.create()
				let prompt = window.prompt
				let name = 'blabla'

				window.prompt = sinon.stub().returns(name)

				server.respondWith('POST', '/login.json',
					[200, { 'Content-Type': 'application/json' },
						(() => {
							o.name = name
							o.users.filter(user =>
								user.oid === o.oid)[0].name = name
							oldMessages[0].name_from = name
							return JSON.stringify(o)
						})()
					])

				nameLink.dispatchEvent(new Event('click'))

				setTimeout(() => {
					server.respond()
					setTimeout(() => {
						assert.equal(
							nameLink.innerHTML,
							name
						)
						window.prompt = prompt
						done()
					}, delay)
				})
			})
		})
		it('should not reset when name length < 3', (done) => {
			setTimeout(() => {
				let nameLink = document.body.querySelector('.login .name')
				let info = document.body.querySelector('.modal .info')
				let prompt = window.prompt
				let name_old = 'blabla'
				let e = new Event('click')

				window.prompt = sinon.stub().returns('')

				nameLink.dispatchEvent(e)

				setTimeout(() => {
					setTimeout(() => {
						assert.equal(
							nameLink.innerHTML,
							name_old
						)
						window.prompt = prompt
						info.dispatchEvent(new Event('click'))
						setTimeout(done)
					}, delay)
				})
			})
		})
	})
	describe('roomlist', () => {
		beforeEach((done) => {
			let server = sinon.fakeServer.create()
			server.respondWith('POST', '/add.json',
					[200, { 'Content-Type': 'application/json' },
						(() => {
							return JSON.stringify(o)
						})()
					]
				)
			toggleMenu('roomlist', () => {
				setTimeout(() => {
					server.respond()
					setTimeout(done, delay)
				})
			})
		})
		afterEach(toggleMenu.bind(null, 'roomlist'))
		describe('+room button', () => {
			it('should create and join room', (done) => {
				setTimeout(() => {
					let btn = document.body.querySelector('.create-room')
					let server = sinon.fakeServer.create()
					let prompt = window.prompt
					let name = 'asd'

					window.prompt = sinon.stub().returns(name)

					server.respondWith('POST', '/add.json',
						[200, { 'Content-Type': 'application/json' },
							(() => {
								o.rooms.push({
									oid: 2,
									name: name,
									inside: true,
									private: false,
									allow: true,
									amount: 1
								})
								return JSON.stringify(o)
							})()
						]
					)

					btn.dispatchEvent(new Event('click'))

					setTimeout(() => {
						server.respond()
						setTimeout(() => {
							let rooms = document.body
								.querySelectorAll('.rooms .inside .name')
							assert.equal(rooms.length, o.rooms.length)
							window.prompt = prompt
							done()
						}, delay)
					})
				})
			})
		})
	})
})

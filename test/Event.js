'use strict'

function Event (name) {
	let e = document.createEvent('Event')

	if (name === 'ctrlEnter') {
		e.initEvent('keydown', true, true)
		e.ctrlKey = true
		e.keyCode = 13
	} else {
		e.initEvent(name, true, true)
	}

	return e
}

export default {Event}

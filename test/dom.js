'use strict'

import assert from 'assert'

describe('dom + vm', () => {
	describe('body', () => {
		it('should contain container', () => {
			assert.ok(document.getElementById('hornchat'))
		})
		it('should contain menu', () => {
			assert.ok(document.body.querySelector('.menu'))
		})
		it('should contain room', () => {
			assert.ok(document.body.querySelector('.room'))
			assert.ok(document.body.querySelector('.messages'))
		})
	})
	it('should have canvas.toBlob', () => {
		let canvas = document.createElement('canvas')
		assert.ok(canvas.toBlob)
	})
	it('should have window.URL', () => {
		assert.ok(window.URL)
	})
	it('should have Set', () => {
		let s = new Set([1, 2, 2, 2])
		assert.equal(s.size, 2)
	})
	it('should have Array.from which converts Set', () => {
		assert.deepEqual(Array.from(new Set([1, 2, 2, 2])), [1, 2])
	})
	// Event object in phantomjs is not a constructor
	it('should have Event', () => {
		assert.ok(Event)
	})
})

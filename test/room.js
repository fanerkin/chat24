'use strict'

import assert from 'assert'
import sinon from 'sinon'
import xtend from 'xtend'
import {response, nextMessage} from './response'
import {Event} from './Event'

let o = response
let mm = o.messages[0]

describe('room', () => {
	describe('name', () => {
		it('should leave on click', (done) => {
			let o = xtend(response)
			o.messages = []

			let roomName = document.body.querySelector('.room_oid_2 > .list')
			let server = sinon.fakeServer.create()

			server.respondWith('POST', '/getout.json',
				[200, { 'Content-Type': 'application/json' },
					(() => {
						o.rooms[1].inside = false
						return JSON.stringify(o)
					})()
				]
			)

			roomName.dispatchEvent(new Event('click'))

			setTimeout(() => {
				server.respond()
				setTimeout(() => {
					let rooms = document.body.querySelectorAll('.room')
					let room = rooms[0]
					assert.equal(
						rooms.length,
						o.rooms.filter(room => room.inside === true).length
					)
					assert.ok(room.classList.contains('room_oid_1'))
					done()
				}, delay)
			})
		})
	})
	describe('sending area', () => {
		it('should send on ctrl + enter', (done) => {
			let ta = document.body.querySelector('.type-message')
			let server = sinon.fakeServer.create()
			let raw = 'blabla'

			mm.html = mm.raw = raw
			server.respondWith('POST', '/send.json', nextMessage())

			ta.value = raw
			ta.dispatchEvent(new Event('ctrlEnter'))

			setTimeout(() => {
				server.respond()
				setTimeout(() => {
					let msgs = document.body.querySelectorAll('.message .text')
					assert.equal(msgs.length, mm.oid)
					assert.equal(
						msgs[msgs.length - 1].innerHTML,
						raw
					)
					done()
				}, delay)
			})
		})
		it.skip('should send on "send" button click', (done) => {
			let ta = document.body.querySelector('.type-message')
			let server = sinon.fakeServer.create()
			let raw = 'baabaa'

			mm.html = mm.raw = raw
			server.respondWith('POST', '/send.json', nextMessage())

			ta.value = raw
			ta.dispatchEvent(new Event('input'))
			setTimeout(() => {
				let btn = document.body.querySelector('.send-message')
				assert.equal(btn.classList.contains('btn'), true)
				btn.dispatchEvent(new Event('click'))

				setTimeout(() => {
					server.respond()
					setTimeout(() => {
						let msgs =
							document.body.querySelectorAll('.message .text')
						assert.equal(msgs.length, mm.oid)
						assert.equal(
							msgs[msgs.length - 1].innerHTML,
							raw
						)
						done()
					}, delay)
				})
			})
		})
	})
})

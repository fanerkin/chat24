'use strict'

import 'babelify/polyfill'
import '../src'

import './dom'
import './menu'
import './room'
import './message'

window.delay = /PhantomJS/.test(navigator.userAgent)
	? 80
	: 160

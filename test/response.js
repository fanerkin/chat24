'use strict'

let oid = 299
let name = 'Vasia'

let response = {
	oid: oid,
	name: name,
	room: 1,
	last_message: 1,
	messages: [
		{
			oid: 1,
			time: 1436726355,
			room: 1,
			appeal: false,
			private: false,
			oid_from: oid,
			name_from: name,
			name_to: '',
			images: [],
			audios: [],
			youtube: [],
			html: 'aaaa',
			raw: 'aaaa'
		}
	],
	users: [
		{
			oid: 0,
			name: 'Robot'
		},
		{
			oid: oid,
			name: name
		}
	],
	rooms: [
		{
			oid: 1,
			name: '#Courtyard',
			inside: true,
			private: false,
			allow: false,
			amount: 5
		}
	],
	images: [],
	audios: [],
	info: '',
	error: ''
}

function nextMessage () {
	let mm = response.messages[0]
	mm.oid++
	mm.time += 4

	return [
		200,
		{ 'Content-Type': 'application/json' },
		JSON.stringify(response)
	]
}

export default {nextMessage, response}

# chat24 interface [![chat24.club](http://chat24.club)](http://chat24.club)

### tech stack
- __deku__
- __jsx__
- __es6__

### util
- __npm__ & __make__ - taskrunner
- __mochify__ - testrunner
- __browserify__ - builder
- __express__ & modules - proxy server

### development under GNU/Linux
- `npm i` to install dependencies
- `npm run build` to build
- `npm run watch` to watch for changes in __*.js__ and __*.css__ files. Performs fast rebuild and headless tests. You should have inotify-tools installed to also update __index.min.js__
- `npm test` to test
- `npm start` to start local proxy then [![localhost:8181](localhost:8181)](http://localhost:8181)

### deployment/restart
- `npm run deploy` to deploy
- `npm run restart` to restart daemon

### License
Copyright © 2015 fanerkin

MIT
